#include "Map.h"
#include "GameLog.h"

Map::Map(char* filePath)
{
	mCamera = new Camera(GameGlobal::GetWidth(), GameGlobal::GetHeight());
	LoadMap(filePath);
}

Map::~Map()
{
	delete mMap;

	for (size_t i = 0; i < mListHakims.size(); i++)
	{
		if (mListHakims[i])
			delete mListHakims[i];
	}
	mListHakims.clear();

	for (size_t i = 0; i < mListBoxers.size(); i++)
	{
		if (mListBoxers[i])
			delete mListBoxers[i];
	}
	mListBoxers.clear();

	for (size_t i = 0; i < mListRazouls.size(); i++)
	{
		if (mListRazouls[i])
			delete mListRazouls[i];
	}
	mListRazouls.clear();

	for (size_t i = 0; i < mListTileset.size(); i++)
	{
		if (mListTileset[i])
			delete mListTileset[i];
	}
	mListTileset.clear();

	delete mQuadTree;
}

void Map::LoadMap(char* filePath)
{
	mMap = new Tmx::Map();
	mMap->ParseFile(filePath);

	RECT r;
	r.left = 0;
	r.top = 0;
	r.right = this->GetWidth();
	r.bottom = this->GetHeight();

	mQuadTree = new QuadTree(1, r);

	for (size_t i = 0; i < mMap->GetNumTilesets(); i++)
	{
		const Tmx::Tileset *tileset = mMap->GetTileset(i);

		Sprite *sprite = new Sprite(tileset->GetImage()->GetSource().c_str());

		mListTileset.insert(std::pair<int, Sprite*>(i, sprite));
	}

	// Khoi tao cac items
	for (size_t i = 0; i < GetMap()->GetNumTileLayers(); i++)
	{
		const Tmx::TileLayer *layer = mMap->GetTileLayer(i);

		if (layer->IsVisible())
			continue;

		if (layer->GetName() == "Hakim" || layer->GetName() == "Knife" || 
			layer->GetName() == "Fazal" || layer->GetName() == "Unknow" || 
			layer->GetName() == "Boxer" || layer->GetName() == "Razoul" || 
			layer->GetName() == "Cawl")
		{
			for (size_t j = 0; j < mMap->GetNumTilesets(); j++)
			{
				const Tmx::Tileset *tileSet = mMap->GetTileset(j);

				int tileWidth = mMap->GetTileWidth();
				int tileHeight = mMap->GetTileHeight();

				int tileSetWidth = tileSet->GetImage()->GetWidth() / tileWidth;
				int tileSetHeight = tileSet->GetImage()->GetHeight() / tileHeight;

				for (size_t m = 0; m < layer->GetHeight(); m++)
				{
					for (size_t n = 0; n < layer->GetWidth(); n++)
					{
						if (layer->GetTileTilesetIndex(n, m) != -1)
						{
							int tileID = layer->GetTileId(n, m);

							int y = tileID / tileSetWidth;
							int x = tileID - y * tileSetWidth;

							RECT sourceRECT;
							sourceRECT.top = y * tileHeight;
							sourceRECT.bottom = sourceRECT.top + tileHeight;
							sourceRECT.left = x * tileWidth;
							sourceRECT.right = sourceRECT.left + tileWidth;

							RECT bound;
							bound.left = n * tileWidth;
							bound.top = m * tileHeight;
							bound.right = bound.left + tileWidth;
							bound.bottom = bound.top + tileHeight;

							D3DXVECTOR3 position(n * tileWidth + tileWidth / 2, m * tileHeight + tileHeight / 2, 0);

							/*GameObject *gameObject = nullptr;

							if (layer->GetName() == "Hakim")
							{
								Hakim *hakim = new Hakim(position);
								hakim->Tag = GameObject::ObjectTypes::Hakim;
								gameObject = hakim;
								mListHakims.push_back(hakim);
							}

							if (gameObject)
								mQuadTree->insertObject(gameObject);*/
						}
					}
				}
			}
		}
	}

#pragma region -OBJECTGROUP, STATIC OBJECT-
	for (size_t i = 0; i < mMap->GetNumObjectGroups(); i++)
	{
		const Tmx::ObjectGroup *objectGroup = mMap->GetObjectGroup(i);

		for (size_t j = 0; j < objectGroup->GetNumObjects(); j++)
		{
			Tmx::Object *object = objectGroup->GetObjects().at(j);

			GameObject *gameObject = new GameObject();
			if (objectGroup->GetName() == "player")
			{
				Aladdin *aladdin = new Aladdin();
				aladdin->SetPosition(D3DXVECTOR3(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2, 0));
				aladdin->SetWidth(object->GetWidth());
				aladdin->SetHeight(object->GetHeight());
				aladdin->Tag = GameObject::ObjectTypes::Aladdin;
				gameObject = aladdin;
				mAladdin = aladdin;
			}
			else if (objectGroup->GetName() == "Hakim")
			{
				Hakim *hakim = new Hakim();
				hakim->init(D3DXVECTOR3(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2, 0));
				hakim->SetWidth(object->GetWidth());
				hakim->SetHeight(object->GetHeight());
				hakim->Tag = GameObject::ObjectTypes::Hakim;
				gameObject = hakim;
				mListHakims.push_back(hakim);
			}
			else if (objectGroup->GetName() == "Boxer")
			{
				Boxer *boxer = new Boxer();
				boxer->init(D3DXVECTOR3(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2, 0));
				boxer->SetWidth(object->GetWidth());
				boxer->SetHeight(object->GetHeight());
				boxer->Tag = GameObject::ObjectTypes::Boxer;
				gameObject = boxer;
				mListBoxers.push_back(boxer);
			}
			else if (objectGroup->GetName() == "Razoul")
			{
				Razoul *razoul = new Razoul();
				razoul->init(D3DXVECTOR3(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2, 0));
				razoul->SetWidth(object->GetWidth());
				razoul->SetHeight(object->GetHeight());
				razoul->Tag = GameObject::ObjectTypes::Razoul;
				gameObject = razoul;
				mListRazouls.push_back(razoul);
			}
			else if (objectGroup->GetName() == "Knife")
			{
				Knife *knife = new Knife();
				knife->init(D3DXVECTOR3(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2, 0));
				knife->SetWidth(object->GetWidth());
				knife->SetHeight(object->GetHeight());
				knife->Tag = GameObject::ObjectTypes::Knife;
				gameObject = knife;
				mListKnifes.push_back(knife);
			}
			else if (objectGroup->GetName() == "Hand")
			{
				Hand *hand = new Hand();
				hand->init(D3DXVECTOR3(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2, 0));
				hand->SetWidth(object->GetWidth());
				hand->SetHeight(object->GetHeight());
				hand->Tag = GameObject::ObjectTypes::Hand;
				gameObject = hand;
				mListHands.push_back(hand);
			}
			else if (objectGroup->GetName() == "Camel")
			{
				Camel *camel = new Camel();
				camel->init(D3DXVECTOR3(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2, 0));
				camel->SetWidth(object->GetWidth());
				camel->SetHeight(object->GetHeight());
				camel->Tag = GameObject::ObjectTypes::Camel;
				gameObject = camel;
				mListCamels.push_back(camel);
			}
			else if (objectGroup->GetName() == "Fire")
			{
				gameObject->SetPosition(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2);
				gameObject->SetWidth(object->GetWidth());
				gameObject->SetHeight(object->GetHeight());
				gameObject->Tag = GameObject::ObjectTypes::Fire;
			}
			else if (objectGroup->GetName() == "Earth")
			{
				gameObject->SetPosition(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2);
				gameObject->SetWidth(object->GetWidth());
				gameObject->SetHeight(object->GetHeight());
				gameObject->Tag = GameObject::ObjectTypes::Earth;
			}
			else if (objectGroup->GetName() == "Apple")
			{
				gameObject->SetPosition(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2);
				gameObject->SetWidth(object->GetWidth());
				gameObject->SetHeight(object->GetHeight());
				gameObject->Tag = GameObject::ObjectTypes::Apple;
			}
			else if (objectGroup->GetName() == "Wall")
			{
				gameObject->SetPosition(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2);
				gameObject->SetWidth(object->GetWidth());
				gameObject->SetHeight(object->GetHeight());
				gameObject->Tag = GameObject::ObjectTypes::Wall;
			}
			else if (objectGroup->GetName() == "WoodenLadder")
			{
				gameObject->SetPosition(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2);
				gameObject->SetWidth(object->GetWidth());
				gameObject->SetHeight(object->GetHeight());
				gameObject->Tag = GameObject::ObjectTypes::WoodenLadder;
			}
			else
			{
				gameObject->SetPosition(object->GetX() + object->GetWidth() / 2, object->GetY() + object->GetHeight() / 2);
				gameObject->SetWidth(object->GetWidth());
				gameObject->SetHeight(object->GetHeight());
				gameObject->Tag = GameObject::ObjectTypes::None;
			}

			mQuadTree->insertObject(gameObject);
		}
	}
#pragma endregion
}

void Map::SetCamera(Camera* camera)
{
	mCamera = camera;
	mAladdin->SetCamera(camera);
}

Tmx::Map* Map::GetMap()
{
	return mMap;
}

int Map::GetWidth()
{
	return mMap->GetWidth() * mMap->GetTileWidth();
}

int Map::GetHeight()
{
	return mMap->GetHeight() * mMap->GetTileHeight();
}

int Map::GetTileWidth()
{
	return mMap->GetTileWidth();
}

int Map::GetTileHeight()
{
	return mMap->GetTileHeight();
}

void Map::Update(float dt)
{
	for (size_t i = 0; i < mListHakims.size(); i++)
	{
		mListHakims[i]->Update(dt);
		mListHakims[i]->UpdateCamera(mCamera);
	}
	for (size_t i = 0; i < mListBoxers.size(); i++)
	{
		mListBoxers[i]->Update(dt);
		mListBoxers[i]->UpdateCamera(mCamera);
	}
	for (size_t i = 0; i < mListRazouls.size(); i++)
	{
		mListRazouls[i]->Update(dt);
		mListRazouls[i]->UpdateCamera(mCamera);
	}
}

void Map::Draw()
{
	D3DXVECTOR2 trans = D3DXVECTOR2(GameGlobal::GetWidth() / 2 - mCamera->GetPosition().x,
		GameGlobal::GetHeight() / 2 - mCamera->GetPosition().y);

	for (size_t i = 0; i < mMap->GetNumTileLayers(); i++)
	{
		const Tmx::TileLayer *layer = mMap->GetTileLayer(i);

		if (!layer->IsVisible())
		{
			continue;
		}

		if (i == 1)
		{
			#pragma region DRAW ENEMIES
			for (size_t i = 0; i < mListHakims.size(); i++)
			{
				mListHakims[i]->Draw(trans);
			}
			for (size_t i = 0; i < mListBoxers.size(); i++)
			{
				mListBoxers[i]->Draw(trans);
			}
			for (size_t i = 0; i < mListRazouls.size(); i++)
			{
				mListRazouls[i]->Draw(trans);
			}
			#pragma endregion

			mAladdin->Draw();
		}

		RECT sourceRECT;

		int tileWidth = mMap->GetTileWidth();
		int tileHeight = mMap->GetTileHeight();

		for (size_t m = 0; m < layer->GetHeight(); m++)
		{
			for (size_t n = 0; n < layer->GetWidth(); n++)
			{
				int tilesetIndex = layer->GetTileTilesetIndex(n, m);

				if (tilesetIndex != -1)
				{
					const Tmx::Tileset *tileSet = mMap->GetTileset(tilesetIndex);

					int tileSetWidth = tileSet->GetImage()->GetWidth() / tileWidth;
					int tileSetHeight = tileSet->GetImage()->GetHeight() / tileHeight;

					Sprite* sprite = mListTileset[layer->GetTileTilesetIndex(n, m)];

					//tile index
					int tileID = layer->GetTileId(n, m);

					int y = tileID / tileSetWidth;
					int x = tileID - y * tileSetWidth;

					sourceRECT.top = y * tileHeight;
					sourceRECT.bottom = sourceRECT.top + tileHeight;
					sourceRECT.left = x * tileWidth;
					sourceRECT.right = sourceRECT.left + tileWidth;

					//tru tilewidth/2 va tileheight/2 vi Sprite ve o vi tri giua hinh anh cho nen doi hinh de cho
					//dung toa do (0,0) cua the gioi thuc la (0,0) neu khong thi se la (-tilewidth/2, -tileheigth/2);
					D3DXVECTOR3 position(n * tileWidth + tileWidth / 2, m * tileHeight + tileHeight / 2, 0);

					sprite->SetWidth(tileWidth);
					sprite->SetHeight(tileHeight);

					sprite->Draw(position, sourceRECT, D3DXVECTOR2(), trans);
				}
			}
		}
	}
}

Aladdin* Map::getAladdin()
{
	return mAladdin;
}

QuadTree* Map::GetQuadTree()
{
	return mQuadTree;
}
