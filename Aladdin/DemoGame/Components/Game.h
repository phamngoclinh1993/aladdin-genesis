#ifndef __GAME__
#define __GAME__

#include <d3dx9.h>
#include <d3d9.h>
#include <Windows.h>
#include <dinput.h>

#include "GameGlobal.h"
#include "GameTime.h"
#include "Animation.h"

#include "../Managements/SceneManagement.h"
#include "../Scenes/SceneLevelOne.h"

class Game
{
public:
	Game(int fps = 60);
	~Game();

protected:

	PDIRECT3DSURFACE9       mBackground,
		mBackBuffer;

	int                     mWidth,			// Screen width
		mHeight;		// Screen height

	static int              mIsDone;		// Check game is running

	float                   mFPS;			// Count frame per second

	void Init();							// Init game loop	

	void Render();							// Draw game

	void Update(float dt);					// Update game
};

#endif

