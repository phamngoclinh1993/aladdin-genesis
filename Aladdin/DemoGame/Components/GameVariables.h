#pragma once
#ifndef __GLOBAL_VARIABLES__
#define __GLOBAL_VARIABLES__

#define SCREEN_WIDTH					330
#define SCREEN_HEIGHT					240

#define	GAME_TITLE						L"Aladdin Genesis"
#define GAME_CLASS						L"Aladdin Genesis"

#define FPS								60

#define KEYBOARD_BUFFERD_SIZE			1024

#define MAX_BULLET_APPLE				1

#endif