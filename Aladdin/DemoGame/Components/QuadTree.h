#pragma once

#include <d3dx9.h>
#include <d3d9.h>
#include <vector>
#include "Collision.h"
#include "Camera.h"
#include "..\GameObjects\GameObject.h"

class QuadTree
{
public:

	QuadTree(int level, RECT bound);
	~QuadTree();
	void Clear();
	void insertObject(GameObject *object);

	/*lay danh sach nhung Entity co kha nang xay ra va cham
	tra ve danh sach cac phan tu nam trong vung va cham */
	void getObjectsCollideAble(std::vector<GameObject*> &objectsOut, GameObject *object);

	void getAllObjects(std::vector<GameObject*> &objectsOut);

	int getTotalObjects();

	RECT Bound;

protected:
	QuadTree **Nodes;
	std::vector<GameObject*> mListObjects; //danh sach cac phan tu co trong vung va cham (Bound)

	/*lay vi tri cua Object
	0: nam trong Node con goc trai tren
	1: nam trong Node con goc phai tren
	2: nam trong Node con goc trai duoi
	3: nam trong Node con goc phai duoi
	-1: bi dinh > 2 node con*/
	int getIndex(RECT body);

	void split(); //thuc hien chia ra cac node

	bool isContain(GameObject *object);
	int mLevel; //tuong ung voi so node
};