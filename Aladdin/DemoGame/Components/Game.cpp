#include "Game.h"

Game::Game(int fps)
{
	mFPS = fps;

	SceneManagement::GetInstance()->ReplaceScene(new SceneLevelOne());

	Init();
}

Game::~Game()
{

}

void Game::Update(float dt)
{
	SceneManagement::GetInstance()->Update(dt);
	Render();
}

void Game::Render()
{
	auto device = GameGlobal::GetCurrentDevice();

	device->Clear(0, NULL, D3DCLEAR_TARGET, 0x4866ff, 0.0f, 0);

	// Begin scene
	if (device->BeginScene())
	{
		// Begin draw
		GameGlobal::GetCurrentSpriteHandler()->Begin(D3DXSPRITE_ALPHABLEND);

		// Draw
		SceneManagement::GetInstance()->GetCurrentScene()->Draw();

		// End draw
		GameGlobal::GetCurrentSpriteHandler()->End();

		// End scene
		device->EndScene();
	}

	device->Present(0, 0, 0, 0);
}

void Game::Init()
{
	MSG msg;

	float tickPerFrame = 1.0f / mFPS, delta = 0;

	while (GameGlobal::isGameRunning)
	{
		GameTime::GetInstance()->StartCounter();

		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		delta += GameTime::GetInstance()->GetCouter();

		if (delta >= tickPerFrame)
		{
			Update((delta));
			delta = 0;
		}
		else
		{
			Sleep(tickPerFrame - delta);
			delta = tickPerFrame;
		}
	}
}