#include "Collision.h"


Collision::Collision()
{
}


Collision::~Collision()
{
}

GameObject::CollisionReturn Collision::RecteAndRect(RECT rect1, RECT rect2)
{
	GameObject::CollisionReturn result;

	if (!isCollide(rect1, rect2))
	{
		result.IsCollided = false;

		return result;
	}

	result.IsCollided = true;

	//chon max Left
	result.RegionCollision.left = rect1.left > rect2.left ? rect1.left : rect2.left;
	//chon max right
	result.RegionCollision.right = rect1.right < rect2.right ? rect1.right : rect2.right;
	//chon min bottom
	result.RegionCollision.bottom = rect1.bottom < rect2.bottom ? rect1.bottom : rect2.bottom;
	//chon max top
	result.RegionCollision.top = rect1.top > rect2.top ? rect1.top : rect2.top;

	return result;
}

bool Collision::PointAndRectangle(float x, float y, RECT rect)
{
	if (x < rect.left || x > rect.right || y < rect.top || y > rect.bottom)
		return false;

	return true;
}

bool Collision::RectangleAndCircle(RECT rect, int circlex, int circley, int circleRadius)
{
	int px = circlex;
	int py = circley;

	if (px < rect.left)
		px = rect.left;
	else if (px > rect.right)
		px = rect.right;

	if (py > rect.bottom)
		py = rect.bottom;
	else if (py < rect.top)
		py = rect.top;

	int dx = px - circlex;
	int dy = py - circley;

	return (dx * dx + dy * dy) <= circleRadius * circleRadius;
}

bool Collision::isCollide(RECT rect1, RECT rect2)
{
	if (rect1.left > rect2.right || rect1.right < rect2.left || rect1.top > rect2.bottom || rect1.bottom < rect2.top)
	{
		return false;
	}

	return true;
}

GameObject::SideCollisions Collision::checkCollision(GameObject *currentObject, GameObject *otherObject)
{
	float normalx, normaly;
	float collisionTime;
	float moveX, moveY;
	D3DXVECTOR2 tmpVelocity;

	if (AABB(currentObject->GetObjectBox(), otherObject->GetObjectBox(), moveX, moveY) == false)
	{
		if (currentObject->GetVx() != 0.0 || currentObject->GetVy() != 0.0)
		{
			tmpVelocity = D3DXVECTOR2(currentObject->GetVx() - otherObject->GetVx(),
										currentObject->GetVy() - otherObject->GetVy());
		}
		
		// start check collision
		if (AABB(ConvertRectToBoardBox(currentObject->GetBound(), tmpVelocity.x, tmpVelocity.y),
					otherObject->GetObjectBox(), moveX, moveY) == true) // dong
		{
			collisionTime = SweptAABB(
				ConvertRectToBox(currentObject->GetBound(), currentObject->GetVx(), currentObject->GetVy()),
				otherObject->GetObjectBox(),
				normalx, normaly
			);

			if (collisionTime > 0 && collisionTime < 1)
			{
				if (normaly != 0)
				{
					if (moveY != 0)
					{
						if (normaly == 1.0f)
						{
							return GameObject::SideCollisions::Top;
						}
						else if (normaly == -1.0f)
						{
							return GameObject::SideCollisions::Bottom;
						}
					}
				}
				else
				{
					if (moveX != 0)
					{
						if (normalx == -1.0f)
						{
							return GameObject::SideCollisions::Left;
						}
						else if (normalx == 1.0f)
						{
							return GameObject::SideCollisions::Right;
						}
					}
				}
			}
		}
		else
		{
			if (AABBCheckCustom(
				ConvertRectToBoardBox(currentObject->GetBound(),
										currentObject->GetVx(),
										currentObject->GetVy()),
				otherObject->GetObjectBox()) == true)
			{
				return GameObject::SideCollisions::Top;
			}
		}
	}

	return GameObject::SideCollisions::NotKnow;
}

GameObject::SideCollisions Collision::getSideCollision(GameObject *object1, GameObject *object2)
{
	RECT rect1 = object1->GetBound();
	RECT rect2 = object2->GetBound();

	float w = (object1->GetWidth() + object2->GetWidth()) / 2.0f;
	float h = (object1->GetHeight() + object2->GetHeight()) / 2.0f;

	float dx = object1->GetPosition().x - object2->GetPosition().y;
	float dy = object1->GetPosition().x - object2->GetPosition().y;

	if (abs(dx) <= w && abs(dy) <= h)
	{
		/* co va cham*/
		float wy = w * dy;
		float hx = h * dx;

		if (wy > hx)
		{
			if (wy > -hx)
			{
				/*va cham phia tren e1*/
				return GameObject::Top;
			}
			else
			{
				/*va cham phia ben phai e1*/
				return GameObject::Right;
			}
		}
		else if (wy > -hx)
		{
			/*va cham ben trai e1*/
			return GameObject::Left;
		}
		else
		{
			/*va cham phia duoi e1*/
			return GameObject::Bottom;
		}
	}
}

GameObject::SideCollisions Collision::getSideCollision(GameObject *object, GameObject::CollisionReturn data)
{
	float xCenter = data.RegionCollision.left + (data.RegionCollision.right - data.RegionCollision.left) / 2.0f;
	float yCenter = data.RegionCollision.top + (data.RegionCollision.bottom - data.RegionCollision.top) / 2.0f;

	D3DXVECTOR2 cCenter = D3DXVECTOR2(xCenter, yCenter);
	D3DXVECTOR2 eCenter = object->GetPosition();

	//lay vector noi tam Entity va CollisionRect
	D3DXVECTOR2 vec = cCenter - eCenter;

	//chuan hoa vector
	D3DXVec2Normalize(&vec, &vec);

	/*
	- neu vector chuan hoa co y > 0 =>nam phia ben tren Entity
	- neu vector chuan hoa co y < 0 =>nam phia ben duoi Entity
	- neu vector chuan hoa co x > 0 => nam phia ben phai Entity
	- neu vector chuan hoa co x < 0 => nam phia ben trai Entity
	*/

	if (vec.y < 0)
	{
		//va cham phia ben tren
		//lay cos cua goc neu ma nam trong khoang goc 70 -> 110 thi va cham top
		if (vec.x <= 0.35f && vec.x >= -0.35f)
		{
			return GameObject::Top;
		}
		else if (vec.x > 0.35f && vec.x < 0.8f)
		{
			//goc trong khoang 35 -> 70 phia ben top - right
			return GameObject::TopRight;
		}
		else if (vec.x >= 0.8f)
		{
			return GameObject::Right;
		}
		else if (vec.x < -0.35f && vec.x >= -0.8f)
		{
			//va cham phia top - left
			return GameObject::TopLeft;
		}
		else
		{
			return GameObject::Left;
		}
	}
	else
	{
		//va cham phia ben duoi
		//lay cos cua goc neu ma nam trong khoang goc 70 -> 110 thi va cham top
		if (vec.x <= 0.35f && vec.x >= -0.35)
		{
			return GameObject::Bottom;
		}
		else if (vec.x > 0.35 && vec.x < 0.8)
		{
			//goc trong khoang 35 -> 70 phia ben top - right
			return GameObject::BottomRight;
		}
		else if (vec.x >= 0.8)
		{
			return GameObject::Right;
		}
		else if (vec.x < -0.35f && vec.x > -0.8f)
		{
			//va cham phia top - left
			return GameObject::BottomLeft;
		}
		else
		{
			return GameObject::Left;
		}
	}

	return GameObject::NotKnow;
}