#pragma once
#ifndef __MAP__
#define __MAP__

#include <d3dx9.h>
#include <d3d9.h>
#include <vector>

#include "Sprite.h"
#include "..\MapReader\Tmx.h.in"
#include "GameGlobal.h"
#include "Camera.h"
#include "QuadTree.h"
#include "..\GameObjects\Aladdin\Aladdin.h"
#include "..\GameObjects\Enemies\Hakim\Hakim.h"
#include "..\GameObjects\Enemies\Boxer\Boxer.h"
#include "..\GameObjects\Enemies\Razoul\Razoul.h"
#include "..\GameObjects\Enemies\Hand\Hand.h"
#include "..\GameObjects\Enemies\Knife\Knife.h"
#include "..\GameObjects\Enemies\Camel\Camel.h"

class Map
{
public:
	Map(char* filePath);

	Tmx::Map* GetMap();

	int GetWidth();
	int GetHeight();
	int GetTileWidth();
	int GetTileHeight();

	void SetCamera(Camera *camera);

	void Update(float dt);
	void Draw();

	bool IsBoundLeft(); //kiem tra luc nay Camera o vi bien ben trai so voi WorldMap
	bool IsBoundRight(); // kiem tra xem co o vi tri bien ben phai worldmap khong
	bool IsBoundTop(); // kiem tra xem co o vi tri bien ben trai worldmap khong
	bool IsBoundBottom(); // kiem tra xem co o vi tri bien ben phai worldmap khong

	RECT GetWorldMapBound();

	QuadTree* GetQuadTree();

	~Map();

	Aladdin *getAladdin();

private:
	void LoadMap(char* filePath);

    bool isContain(RECT rect1, RECT rect2);

    Tmx::Map                        *mMap;
    std::map<int, Sprite*>          mListTileset;
    Camera							*mCamera;
	QuadTree                        *mQuadTree;

	Aladdin							*mAladdin;

    std::vector<Sprite*>            mListBricks;
	std::vector<Hakim*>             mListHakims;
	std::vector<Boxer*>             mListBoxers;
	std::vector<Razoul*>            mListRazouls;
	std::vector<Hand*>				mListHands;
	std::vector<Knife*>             mListKnifes;
	std::vector<Camel*>				mListCamels;
};

#endif

