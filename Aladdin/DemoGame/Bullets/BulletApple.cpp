#include "BulletApple.h"
#include "..\Components\GameLog.h"
#include "..\Components\Collision.h"


BulletApple::BulletApple()
{
	direction = 1;
}

BulletApple::BulletApple(D3DXVECTOR3 position)
{
	init(position);
}

BulletApple::~BulletApple()
{
}

bool BulletApple::init(D3DXVECTOR3 position)
{
	_isDead = false;

	SetPosition(position);

	mAnimation = new Animation("Resources/Items/bullet_apple.png", 1, 1, 1, 0.35f);

	this->vx = 5;
	this->vy = 1;

	return true;
}

void BulletApple::setCamera(Camera *camera)
{
	this->mCamera = camera;
}

RECT BulletApple::GetBound()
{
	RECT rect;
	rect.left = this->posX - mAnimation->GetWidth() / 2;
	rect.right = rect.left + mAnimation->GetWidth();
	rect.top = this->posY - mAnimation->GetHeight() / 2;
	rect.bottom = rect.top + mAnimation->GetHeight();

	return rect;
}

void BulletApple::Update(float dt)
{
	if (!_isDead)
	{
		mAnimation->Update(dt);

		time += 2.0;

		float X = time;
		float Y = (9.8f * X * X) / 225;

		this->vx += (X * 8.0) * (direction);
		this->vy += Y;

		GameObject::Update(dt);
	}
}

void BulletApple::Draw(D3DXVECTOR3 position, RECT sourceRect, D3DXVECTOR2 scale, D3DXVECTOR2 transform, float angle, D3DXVECTOR2 rotationCenter, D3DXCOLOR colorKey)
{
	if (!_isDead)
	{
		mAnimation->SetPosition(this->GetPosition());
		mAnimation->FlipVertical(mCurrentReverse);

		if (mCamera)
		{
			D3DXVECTOR2 trans = D3DXVECTOR2(GameGlobal::GetWidth() / 2 - mCamera->GetPosition().x,
				GameGlobal::GetHeight() / 2 - mCamera->GetPosition().y);

			mAnimation->Draw(D3DXVECTOR3(posX, posY, 0), sourceRect, scale, trans, angle, rotationCenter, colorKey);
		}
		else
		{
			mAnimation->Draw(D3DXVECTOR3(posX, posY, 0));
		}
	}
}

void BulletApple::Draw(D3DXVECTOR2 transform)
{
	if (!_isDead)
	{
		mAnimation->SetPosition(this->GetPosition());
		mAnimation->FlipVertical(mCurrentReverse);
		mAnimation->Draw(D3DXVECTOR2(transform));
	}
}

void BulletApple::OnCollision(GameObject *impactor, CollisionReturn data, SideCollisions side)
{
	if (impactor->Tag != GameObject::ObjectTypes::Aladdin
		&& impactor->Tag != GameObject::ObjectTypes::None) {
		this->_isDead = true;
	}
}
