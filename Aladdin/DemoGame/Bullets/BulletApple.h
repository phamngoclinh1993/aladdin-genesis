#pragma once
#include "Bullet.h"

class BulletApple : public Bullet
{
public:
	BulletApple();
	~BulletApple();

	BulletApple(D3DXVECTOR3 position);

	bool init(D3DXVECTOR3 position);

	void setCamera(Camera *camera);

	RECT GetBound();

	void Update(float dt);

	void Draw(D3DXVECTOR3 position = D3DXVECTOR3(), RECT sourceRect = RECT(), D3DXVECTOR2 scale = D3DXVECTOR2(), D3DXVECTOR2 transform = D3DXVECTOR2(), float angle = 0, D3DXVECTOR2 rotationCenter = D3DXVECTOR2(), D3DXCOLOR colorKey = D3DCOLOR_XRGB(255, 255, 255));

	void Draw(D3DXVECTOR2 transform);

	void OnCollision(GameObject *impactor, CollisionReturn data, SideCollisions side);

	int direction;

protected:
	float time = 0.0f;
};

