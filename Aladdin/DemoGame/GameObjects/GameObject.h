#pragma once

#include <d3dx9.h>
#include <d3d9.h>
#include <map>
#include "SweptAABB.h"
#include "..\Components\Camera.h"

static Box ConvertRectToBox(RECT rect, float vx = 0.0f, float vy = 0.0f)
{
	Box box(
		rect.left,
		rect.top,
		rect.right - rect.left,
		rect.bottom - rect.top,
		vx,
		vy
		);

	return box;
}

static Box ConvertRectToBoardBox(RECT rect, float vx = 0.0f, float vy = 0.0f)
{
	return GetSweptBroadphaseBox(ConvertRectToBox(rect, vx, vy));
}

class GameObject
{
public:
	GameObject();
	~GameObject();

	enum SideCollisions
	{
		Left, //0
		Right, //1
		Top, //2
		Bottom, //3
		TopLeft, //4
		TopRight, //5
		BottomLeft, //6
		BottomRight, //7
		NotKnow
	};

	struct CollisionReturn
	{
		bool IsCollided;
		RECT RegionCollision;
	};

	enum ObjectTypes
	{
		None,

		// Bullet
		Bullet,

		// Player
		Aladdin,

		// Enemies
		Camel,
		Hakim,
		Boxer,
		Razoul,
		Knife,
		Hand,
		Fazal,
		Fire,

		// Static Objects
		Static,
		Earth,
		Rope,
		Wall,
		WoodenLadder,

		// Items
		Apple,
		Ladder,
		Lamp,
		Genie,
		Health,
		Diamond,
		RestartPoint,
		Monkey
	};

	ObjectTypes Tag; //Tag de nhan vien loai Object

	virtual RECT GetBound();

	virtual Box GetObjectBox();

	virtual void SetPosition(float x, float y);

	virtual void SetPosition(D3DXVECTOR2 pos);

	virtual void SetPosition(D3DXVECTOR3 pos);

	virtual void AddPosition(D3DXVECTOR3 pos);

	virtual void AddPosition(D3DXVECTOR2 pos);

	virtual void AddPosition(float x, float y);

	virtual void SetWidth(int width);

	virtual int GetWidth();

	virtual void SetHeight(int height);

	virtual int GetHeight();

	virtual Camera *GetCamera();

	virtual float GetVx();

	virtual void SetVx(float vx);

	virtual void AddVx(float vx);

	virtual float GetVy();

	virtual void SetVy(float vy);

	virtual void AddVy(float vy);

	virtual D3DXVECTOR3 GetPosition();

	virtual void Update(float dt);

	virtual void UpdateCamera(Camera *camera);

	//kiem soat viec va cham
	//khi xay ra va cham voi 1 thuc the nao do thi ham nay se dc goi de xu ly
	virtual void OnCollision(GameObject *impactor, CollisionReturn data, SideCollisions side);

	virtual bool getWasAttacked();
	virtual void setWasAttacked(bool temp);
	virtual void GetAttack();

	virtual bool IsDead();

	virtual void Dead();

	//heath
	int heath = 1;

	// deltaTime
	float deltaTime = 0.0;

protected:

	Camera *mCamera;

	//duoc goi khi set position cua Entity, dung cho ke thua
	virtual void OnSetPosition(D3DXVECTOR3 pos);

	//vi tri tam position x va y
	float posX, posY;

	//phan toc vx, vy
	float vx, vy;

	//size cua entity
	float width, height;

	bool wasAttacked = false;
};

