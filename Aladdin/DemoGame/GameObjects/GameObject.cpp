#include "GameObject.h"

GameObject::GameObject()
{
	Tag = None;
}

GameObject::~GameObject()
{
	
}

D3DXVECTOR3 GameObject::GetPosition()
{
	return D3DXVECTOR3(posX, posY, 0);
}

RECT GameObject::GetBound()
{
	RECT bound;

	bound.left = posX - width / 2;
	bound.right = posX + width / 2;
	bound.top = posY - height / 2;
	bound.bottom = posY + height / 2;

	return bound;
}

Box GameObject::GetObjectBox()
{
	RECT rect = this->GetBound();
	return ConvertRectToBox(rect, vx, vy);
}

void GameObject::OnSetPosition(D3DXVECTOR3 pos)
{

}

void GameObject::Update(float dt)
{
	//velocity = pixel / s
	posX += vx * dt;
	posY += vy * dt;

	deltaTime = dt;
}

void GameObject::UpdateCamera(Camera *camera)
{
	mCamera = camera;
}

void GameObject::OnCollision(GameObject *impactor, CollisionReturn data, SideCollisions side)
{
	vx = 0, vy = 0;
}

void GameObject::SetPosition(float x, float y)
{
	SetPosition(D3DXVECTOR2(x, y));
}

void GameObject::SetPosition(D3DXVECTOR2 pos)
{
	SetPosition(D3DXVECTOR3(pos.x, pos.y, 0));
}

void GameObject::SetPosition(D3DXVECTOR3 pos)
{
	this->posX = pos.x;
	this->posY = pos.y;

	OnSetPosition(pos);
}

void GameObject::AddPosition(D3DXVECTOR3 pos)
{
	this->SetPosition(this->GetPosition() + pos);
}

void GameObject::AddPosition(D3DXVECTOR2 pos)
{
	AddPosition(D3DXVECTOR3(pos));
}

void GameObject::AddPosition(float x, float y)
{
	AddPosition(D3DXVECTOR3(x, y, 0));
}

void GameObject::SetWidth(int width)
{
	this->width = width;
}

int GameObject::GetWidth()
{
	return width;
}

void GameObject::SetHeight(int height)
{
	this->height = height;
}

int GameObject::GetHeight()
{
	return height;
}

Camera* GameObject::GetCamera()
{
	return mCamera;
}

void GameObject::GetAttack()
{
	heath--;
}

bool GameObject::IsDead()
{
	if (heath > 0)
	{
		return false;
	}
	return true;
}

void GameObject::Dead()
{
	if (this->Tag != ObjectTypes::Aladdin)
	{
		this->SetPosition(0, 0);
		this->SetWidth(0);
		this->SetHeight(0);
	}
}

float GameObject::GetVx()
{
	return vx;
}

void GameObject::SetVx(float vx)
{
	this->vx = vx;
}

void GameObject::AddVx(float vx)
{
	this->vx += vx;
}

float GameObject::GetVy()
{
	return vy;
}

void GameObject::SetVy(float vy)
{
	this->vy = vy;
}

void GameObject::AddVy(float vy)
{
	this->vy += vy;
}

bool GameObject::getWasAttacked(){
	return this->wasAttacked;
}
void GameObject::setWasAttacked(bool temp){
	this->wasAttacked = temp;
}
