#pragma once
class Knife;
class KnifeState;

class KnifeData
{
public:
	KnifeData();
	~KnifeData();

	Knife      *knife;
	KnifeState *state;
};

