#pragma once
#include "KnifeState.h"
#include "Knife.h"

class KnifeFightingState : public KnifeState
{
public:
	KnifeFightingState(KnifeData *KnifeData);
	~KnifeFightingState();

	void Update(float dt);

	void KnifeleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	float ellapsedtime;
};
