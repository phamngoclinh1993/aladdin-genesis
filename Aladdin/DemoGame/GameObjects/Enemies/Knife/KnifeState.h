#pragma once

#include "KnifeData.h"
#include "..\..\GameObject.h"
#include <map>

class KnifeState
{
public:
	~KnifeState();

	enum StateName
	{
		Normal,
		Fighting
	};

	virtual void Update(float dt);

	virtual void KnifeleKeyboard(std::map<int, bool> keys);

	//side va cham voi player
	virtual void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	//ham thuan ao bat buoc phai ke thua
	virtual StateName GetState() = 0;

protected:
	KnifeState(KnifeData *KnifeData);
	KnifeState();

	KnifeData *mKnifeData;
};
