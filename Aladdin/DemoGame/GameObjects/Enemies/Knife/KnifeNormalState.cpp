#include "KnifeNormalState.h"
#include "KnifeFightingState.h"


KnifeNormalState::KnifeNormalState(KnifeData *KnifeData)
{
	this->mKnifeData = KnifeData;
}


KnifeNormalState::~KnifeNormalState()
{
}

void KnifeNormalState::Update(float dt)
{
	if (this->mKnifeData->knife->isFighting)
	{
		this->mKnifeData->knife->SetState(new KnifeFightingState(this->mKnifeData));
		return;
	}
}

void KnifeNormalState::KnifeleKeyboard(std::map<int, bool> keys)
{

}

void KnifeNormalState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

KnifeNormalState::StateName KnifeNormalState::GetState()
{
	return KnifeState::Normal;
}