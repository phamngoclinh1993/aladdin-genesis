#include "KnifeFightingState.h"
#include "KnifeNormalState.h"


KnifeFightingState::KnifeFightingState(KnifeData *KnifeData)
{
	this->mKnifeData = KnifeData;
	ellapsedtime = 0.0f;
}


KnifeFightingState::~KnifeFightingState()
{
}

void KnifeFightingState::Update(float dt)
{
	ellapsedtime += dt;
	if (ellapsedtime > 1.0f)
	{
		this->mKnifeData->knife->isFighting = false;
		this->mKnifeData->knife->SetState(new KnifeNormalState(this->mKnifeData));
		return;
	}
}

void KnifeFightingState::KnifeleKeyboard(std::map<int, bool> keys)
{

}

void KnifeFightingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

KnifeFightingState::StateName KnifeFightingState::GetState()
{
	return KnifeState::Fighting;
}