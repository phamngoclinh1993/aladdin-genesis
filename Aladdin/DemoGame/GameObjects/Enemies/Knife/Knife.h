#pragma once
#include "..\..\..\Components\Animation.h"
#include "..\..\GameObject.h"
#include "KnifeData.h"
#include "KnifeState.h"

class Knife : public GameObject
{
public:
	Knife();

	Knife(D3DXVECTOR3 position);

	~Knife();

	bool init(D3DXVECTOR3 position);

	void Update(float dt);

	void Draw(D3DXVECTOR3 position = D3DXVECTOR3(), RECT sourceRect = RECT(), D3DXVECTOR2 scale = D3DXVECTOR2(), D3DXVECTOR2 transform = D3DXVECTOR2(), float angle = 0, D3DXVECTOR2 rotationCenter = D3DXVECTOR2(), D3DXCOLOR colorKey = D3DCOLOR_XRGB(255, 255, 255));

	void Draw(D3DXVECTOR2 transform);

	void SetState(KnifeState *newState);

	void SetReverse(bool flag);

	KnifeState::StateName Knife::getState();

	bool isFighting;

protected:

	KnifeData	*mKnifeData;

	Animation	*mCurrentAnimation,
				*mAnimationNormal,
				*mAnimationFighting;

	bool mCurrentReverse;
	float elapsedTime = 0.0f;

	void changeAnimation(KnifeState::StateName state);

	KnifeState::StateName mCurrentState;
};

