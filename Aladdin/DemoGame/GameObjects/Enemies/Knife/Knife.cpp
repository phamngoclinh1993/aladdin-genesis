#include "Knife.h"
#include "KnifeNormalState.h"
#include "KnifeFightingState.h"

Knife::Knife()
{
	isFighting = false;
}

Knife::Knife(D3DXVECTOR3 position)
{
	init(position);
}

Knife::~Knife()
{
}

bool Knife::init(D3DXVECTOR3 position)
{
	SetPosition(position);

	mAnimationNormal = new Animation("Resources/Enemies/Knife/knife-normal.png", 1, 1, 1, 0.15f);
	mAnimationFighting = new Animation("Resources/Enemies/Knife/knife-attack.png", 6, 1, 6, 0.15f);

	this->mKnifeData = new KnifeData();
	this->mKnifeData->knife = this;
	this->vx = 0;
	this->vy = 0;
	this->SetState(new KnifeNormalState(this->mKnifeData));

	return true;
}


void Knife::Update(float dt)
{
	mCurrentAnimation->Update(dt);

	if (this->mKnifeData->state)
	{
		this->mKnifeData->state->Update(dt);
	}

	GameObject::Update(dt);
}

void Knife::Draw(D3DXVECTOR3 position, RECT sourceRect, D3DXVECTOR2 scale, D3DXVECTOR2 transform, float angle, D3DXVECTOR2 rotationCenter, D3DXCOLOR colorKey)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR3(posX, posY, 0));
}

void Knife::Draw(D3DXVECTOR2 transform)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR2(transform));
}


void Knife::SetReverse(bool flag)
{
	mCurrentReverse = flag;
}


void Knife::SetState(KnifeState *newState)
{
	delete this->mKnifeData->state;

	this->mKnifeData->state = newState;

	this->changeAnimation(newState->GetState());

	mCurrentState = newState->GetState();
}


void Knife::changeAnimation(KnifeState::StateName state)
{
	switch (state)
	{
	case KnifeState::Normal:
		mCurrentAnimation = mAnimationNormal;
		break;

	case KnifeState::Fighting:
		mCurrentAnimation = mAnimationFighting;
		break;
	}

	this->width = mCurrentAnimation->GetWidth();
	this->height = mCurrentAnimation->GetHeight();
}



KnifeState::StateName Knife::getState()
{
	return mCurrentState;
}