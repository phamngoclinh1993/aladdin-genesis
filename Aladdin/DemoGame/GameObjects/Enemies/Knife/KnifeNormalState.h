#pragma once
#include "KnifeState.h"
#include "Knife.h"

class KnifeNormalState : public KnifeState
{
public:
	KnifeNormalState(KnifeData *KnifeData);
	~KnifeNormalState();

	void Update(float dt);

	void KnifeleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();
};
