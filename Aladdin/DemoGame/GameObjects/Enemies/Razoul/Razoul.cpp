#include "Razoul.h"
#include "RazoulAttackState.h"
#include "RazoulRunningState.h"
#include "RazoulWasAttackedState.h"
#include "RazoulStandingState.h"
#include "RazoulDeadState.h"
#include "../../../Components/GameLog.h"

Razoul::Razoul()
{
}

Razoul::Razoul(D3DXVECTOR3 position)
{
	init(position);
}

Razoul::~Razoul()
{
}

bool Razoul::init(D3DXVECTOR3 position)
{
	SetPosition(position);
	mAnimationRunning = new Animation("Resources/Enemies/Razoul/Razoul-runing.png", 8, 1, 8, 0.35f);
	mAnimationAttack = new Animation("Resources/Enemies/Razoul/Razoul-attack2.png", 5, 1, 5, 0.35f);
	mAnimationWasAttacked = new Animation("Resources/Enemies/Razoul/Razoul-was-attack.png", 6, 1, 6, 0.25f);
	mAnimationStanding = new Animation("Resources/Enemies/Razoul/Razoul-standing.png", 14, 1, 14, 0.35f);
	mAnimationWasOnFire = new Animation("Resources/Enemies/Razoul/Razoul-walk-on-fire.png", 9, 1, 9, 0.35f);
	mAnimationWasDead = new Animation("Resources/Enemies/Enemy-Explosions.png", 5, 1, 5, 0.25f);

	this->mRazoulData = new RazoulData();
	this->mRazoulData->razoul = this;
	this->vx = 0;
	this->vy = 0;
	this->oldPosition = this->GetPosition();
	this->SetState(new RazoulStandingState(this->mRazoulData));
	this->heath = 200;
	return true;
}

D3DXVECTOR3 Razoul::GetOldPosition(){
	return this->oldPosition;
}


void Razoul::Update(float dt)
{
	if (!IsDead())
	{
		mCurrentAnimation->Update(dt);
		if (this->mRazoulData->state)
		{
			this->mRazoulData->state->Update(dt);
		}

		GameObject::Update(dt);
	}
	else {
		Dead();
	}
}

void Razoul::Draw(D3DXVECTOR3 position, RECT sourceRect, D3DXVECTOR2 scale, D3DXVECTOR2 transform, float angle, D3DXVECTOR2 rotationCenter, D3DXCOLOR colorKey)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR3(posX, posY, 0));
}

void Razoul::Draw(D3DXVECTOR2 transform)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR2(transform));
}


void Razoul::SetReverse(bool flag)
{
	mCurrentReverse = flag;
}

void Razoul::OnCollision(GameObject *impactor, GameObject::CollisionReturn data, GameObject::SideCollisions side){

	this->mRazoulData->state->OnCollision(impactor, side, data);
	
}

void Razoul::GetAttack()
{
	

	if (heath==2)
	{
		heath = 1;
		this->SetVx(0);
		this->SetState(new RazoulDeadState(this->mRazoulData));
	}
	else if (heath!=1)
	{
		this->heath--;
		this->SetVx(0);
		this->SetState(new RazoulWasAttackedState(this->mRazoulData));
	}
}

void Razoul::SetState(RazoulState *newState)
{
	delete this->mRazoulData->state;

	this->mRazoulData->state = newState;

	this->changeAnimation(newState->GetState());

	mCurrentState = newState->GetState();
}


void Razoul::changeAnimation(RazoulState::StateName state)
{
	switch (state)
	{
	case RazoulState::Running:
		mCurrentAnimation = mAnimationRunning;
		break;

	case RazoulState::Attack:
		mCurrentAnimation = mAnimationAttack;
		break;

	case RazoulState::WasAttacked:
		mCurrentAnimation = mAnimationWasAttacked;
		break;
	case RazoulState::Standing:
		mCurrentAnimation = mAnimationStanding;
		break;

	case RazoulState::WalkOnFire:
		mCurrentAnimation = mAnimationWasOnFire;
		break;
	case RazoulState::Die:
		mCurrentAnimation = mAnimationWasDead;
		break;
	}

	this->width = mCurrentAnimation->GetWidth();
	this->height = mCurrentAnimation->GetHeight();
}



RazoulState::StateName Razoul::getState()
{
	return mCurrentState;
}