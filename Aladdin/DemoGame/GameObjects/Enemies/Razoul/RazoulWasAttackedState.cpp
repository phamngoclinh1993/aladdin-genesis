#include "RazoulWasAttackedState.h"
#include "RazoulStandingState.h"
#include "RazoulDeadState.h"
#include "../../../Components/GameLog.h"

RazoulWasAttackedState::RazoulWasAttackedState(RazoulData *razoulData)
{
	this->mRazoulData = razoulData;
}


void RazoulWasAttackedState::Update(float dt)
{

	if (!this->mRazoulData->razoul->IsDead())
	{
		
		if (elapsedTime > 1.0f)
		{
			GAMELOG("------------a-------------");

			elapsedTime = 0.0f;

			this->mRazoulData->razoul->SetVx(0);

			this->mRazoulData->razoul->SetState(new RazoulStandingState(this->mRazoulData));
		}
		elapsedTime += dt;
	}
	/*else
	{
		this->mRazoulData->razoul->SetVx(0);

		this->mRazoulData->razoul->SetState(new RazoulDeadState(this->mRazoulData));
	}*/
}

void RazoulWasAttackedState::HandleKeyboard(std::map<int, bool> keys)
{

}

void RazoulWasAttackedState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

RazoulWasAttackedState::StateName RazoulWasAttackedState::GetState()
{
	return RazoulState::WasAttacked;
}

RazoulWasAttackedState::~RazoulWasAttackedState()
{
}

