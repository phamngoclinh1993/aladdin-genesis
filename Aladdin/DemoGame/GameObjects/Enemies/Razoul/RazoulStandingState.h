#pragma once
#include"Razoul.h"
#include "RazoulState.h"
class RazoulStandingState:public RazoulState
{
public:
	RazoulStandingState(RazoulData *razoulData);
	~RazoulStandingState();
	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();
};

