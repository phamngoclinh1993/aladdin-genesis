#include "RazoulStandingState.h"
#include "RazoulRunningState.h"
#include "RazoulAttackState.h"
#include "../../../Components/GameLog.h"


RazoulStandingState::RazoulStandingState(RazoulData *razoulData)
{
	this->mRazoulData = razoulData;
}


void RazoulStandingState::Update(float dt)
{
	if (this->mRazoulData->razoul->GetCamera() != NULL)
	{
		
		float tama = abs(this->mRazoulData->razoul->GetPosition().x - this->mRazoulData->razoul->GetOldPosition().x);
		float tam = this->mRazoulData->razoul->GetPosition().x - this->mRazoulData->razoul->GetCamera()->GetPosition().x;
		if (tama > 100.0f && tam*tama >0.0f )
		{
			if (abs(tam)<50)
			{
				this->mRazoulData->razoul->SetVx(0);

				this->mRazoulData->razoul->SetState(new RazoulAttackState(this->mRazoulData));
			}
			return;
		}
		if (tam<200.0f&&tam>10.0f)
		{
			this->mRazoulData->razoul->SetVx(0);

			this->mRazoulData->razoul->SetState(new RazoulRunningState(this->mRazoulData));
			return;
		}
		else if (tam>-200.0f&&tam<-10.0f)
		{
			this->mRazoulData->razoul->SetReverse(true);
			this->mRazoulData->razoul->SetVx(0);

			this->mRazoulData->razoul->SetState(new RazoulRunningState(this->mRazoulData));
			return;
		}
		
	}

}

void RazoulStandingState::HandleKeyboard(std::map<int, bool> keys)
{

}

void RazoulStandingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

RazoulStandingState::StateName RazoulStandingState::GetState()
{
	return RazoulState::Standing;
}

RazoulStandingState::~RazoulStandingState()
{
}
