#pragma once
#include "RazoulState.h"
#include "Razoul.h"
class RazoulRunningState : public RazoulState
{
public:
	RazoulRunningState(RazoulData *razoulData);
	~RazoulRunningState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	//do bien tien van toc sau moi frame tinh bang pixel / s
	float acceleratorX;
	float elapsedTime = 0.0f;
};

