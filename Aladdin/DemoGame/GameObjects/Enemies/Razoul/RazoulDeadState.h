#pragma once
#include"Razoul.h"
#include "RazoulState.h"
class RazoulDeadState : public RazoulState
{
public:
	RazoulDeadState(RazoulData *razoulData);
	~RazoulDeadState();
	void Update(float dt);
	virtual StateName GetState();
	float elapsedTime = 0.0f;
};

