#include "RazoulRunningState.h"
#include "RazoulAttackState.h"
#include "RazoulStandingState.h"
#include "../../../Components/GameLog.h"
#include <math.h>

RazoulRunningState::RazoulRunningState(RazoulData *razoulData)
{
	this->mRazoulData = razoulData;
	acceleratorX = 5.0f;
}


void RazoulRunningState::Update(float dt)
{
	//mHakimData->hakim->SetReverse(true);
	float tam = -this->mRazoulData->razoul->GetPosition().x + this->mRazoulData->razoul->GetCamera()->GetPosition().x;
	float tama = this->mRazoulData->razoul->GetPosition().x - this->mRazoulData->razoul->GetOldPosition().x;
	GAMELOG("%f\t%f", tama,tam);
	if (abs(tama)>100.0f && tam*tama >0.0f)
	{
		this->mRazoulData->razoul->SetVx(0);

		this->mRazoulData->razoul->SetState(new RazoulStandingState(this->mRazoulData));
		return;
	}
	if (tam > 0.0f)
	{

		this->mRazoulData->razoul->SetReverse(true);
		if ((this->mRazoulData->razoul->GetVx() < 20.0f ))
		{
			this->mRazoulData->razoul->AddVx(acceleratorX);

			if (this->mRazoulData->razoul->GetVx() > 20.0f)
			{
				this->mRazoulData->razoul->SetVx(20.0f);
			}
		}
		if (this->mRazoulData->razoul->GetCamera() != NULL)
		{
			if (abs(tam)<50)
			{

				this->mRazoulData->razoul->SetVx(0);

				this->mRazoulData->razoul->SetState(new RazoulAttackState(this->mRazoulData));
			}

		}
		return;
	}
	else
	{
		//di chuyen sang trai
		this->mRazoulData->razoul->SetReverse(false);

		if ((this->mRazoulData->razoul->GetVx() > -20.0f) )
		{
			this->mRazoulData->razoul->AddVx(-acceleratorX);

			if (this->mRazoulData->razoul->GetVx() < -20.0f)
			{
				this->mRazoulData->razoul->SetVx(-20.0f);
			}
		}
		if (abs(tam)<50)
		{
			this->mRazoulData->razoul->SetVx(0);

			this->mRazoulData->razoul->SetState(new RazoulAttackState(this->mRazoulData));
		}
		return;
	}
	
}

void RazoulRunningState::HandleKeyboard(std::map<int, bool> keys)
{

}

void RazoulRunningState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

RazoulRunningState::StateName RazoulRunningState::GetState()
{
	return RazoulState::Running;
}

RazoulRunningState::~RazoulRunningState()
{
}
