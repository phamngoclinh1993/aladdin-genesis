#pragma once
#include "RazoulState.h"
#include "Razoul.h"
class RazoulWasAttackedState : public RazoulState
{
public:
	RazoulWasAttackedState(RazoulData *razoulData);
	~RazoulWasAttackedState();
	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

	float elapsedTime = 0.0f;

};

