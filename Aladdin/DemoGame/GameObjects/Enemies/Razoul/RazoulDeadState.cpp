#include "RazoulDeadState.h"
#include "../../../Components/GameLog.h"

RazoulDeadState::RazoulDeadState(RazoulData *razoulData)
{
	this->mRazoulData = razoulData;
}

void RazoulDeadState::Update(float dt)
{
	if (elapsedTime > 1.0f)
	{
		elapsedTime = 0;
		mRazoulData->razoul->heath = 0;
		GAMELOG("---------------Chet-------------------");
	}
	else
	{
		elapsedTime += dt;
		GAMELOG("elapsedTime= %f", elapsedTime);
	}
}
RazoulDeadState::StateName RazoulDeadState::GetState()
{
	return RazoulState::Die;
}

RazoulDeadState::~RazoulDeadState()
{
}
