#include "RazoulAttackState.h"
#include "RazoulRunningState.h"
#include "../../../Components/GameLog.h"
#include <math.h>

RazoulAttackState::RazoulAttackState(RazoulData *razoulData)
{
	this->mRazoulData = razoulData;
}
RazoulAttackState::~RazoulAttackState()
{
}


void RazoulAttackState::Update(float dt)
{
	float tam = this->mRazoulData->razoul->GetPosition().x - this->mRazoulData->razoul->GetCamera()->GetPosition().x;
	//GAMELOG("Razoul %f", abs(tam));
	if (abs(tam)>50.0f)
	{
		this->mRazoulData->razoul->SetVx(0);

		this->mRazoulData->razoul->SetState(new RazoulRunningState(this->mRazoulData));

		return;
	
	}


	
	
}

void RazoulAttackState::HandleKeyboard(std::map<int, bool> keys)
{
	
}

void RazoulAttackState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

	if (impactor->Tag == GameObject::ObjectTypes::Aladdin)
	{
		
		if (!impactor->IsDead())
		{
			impactor->GetAttack();
		}
	
	}
	
}

RazoulAttackState::StateName RazoulAttackState::GetState()
{
	return RazoulState::Attack;
}

