#pragma once
#include "..\..\..\Components\Animation.h"
#include "..\..\GameObject.h"
#include "RazoulData.h"
#include "RazoulState.h"
class Razoul : public GameObject
{
public:
	Razoul();

	Razoul(D3DXVECTOR3 position);

	~Razoul();

	bool init(D3DXVECTOR3 position);

	void Update(float dt);

	void Draw(D3DXVECTOR3 position = D3DXVECTOR3(), RECT sourceRect = RECT(), D3DXVECTOR2 scale = D3DXVECTOR2(), D3DXVECTOR2 transform = D3DXVECTOR2(), float angle = 0, D3DXVECTOR2 rotationCenter = D3DXVECTOR2(), D3DXCOLOR colorKey = D3DCOLOR_XRGB(255, 255, 255));

	void Draw(D3DXVECTOR2 transform);

	void SetState(RazoulState *newState);

	void SetReverse(bool flag);

	D3DXVECTOR3 GetOldPosition();

	void GetAttack();


	void OnCollision(GameObject *impactor, GameObject::CollisionReturn data, GameObject::SideCollisions side);

	RazoulState::StateName Razoul::getState();

protected:

	RazoulData *mRazoulData;

	Animation	*mCurrentAnimation,
		*mAnimationRunning,
		*mAnimationAttack,
		*mAnimationStanding,
		*mAnimationWasAttacked,
		*mAnimationWasOnFire,
		*mAnimationWasDead;

	bool mCurrentReverse;
	bool mCanMoveLeft;
	bool mCanMoveRight;

	float elapsedTime = 0.0f;
	D3DXVECTOR3 oldPosition;

	void changeAnimation(RazoulState::StateName state);

	RazoulState::StateName mCurrentState;

	//do bien tien van toc sau moi frame tinh bang pixel / s
	float acceleratorX;
};

