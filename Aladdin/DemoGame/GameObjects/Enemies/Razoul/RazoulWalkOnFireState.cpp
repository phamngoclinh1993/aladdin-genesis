#include "RazoulWalkOnFireState.h"
#include "RazoulAttackState.h"

RazoulWalkOnFireState::RazoulWalkOnFireState(RazoulData *razoulData)
{
	this->mRazoulData = razoulData;
	acceleratorX = 5.0f;
}


void RazoulWalkOnFireState::Update(float dt)
{
	//mHakimData->hakim->SetReverse(true);


	if (elapsedTime > 5.0f)
	{
		this->mRazoulData->razoul->SetVx(0);
		this->mRazoulData->razoul->SetState(new RazoulAttackState(this->mRazoulData));
		return;
	}
	else
	{
		//di chuyen sang trai
		if (this->mRazoulData->razoul->GetVx() > -20.0f)
		{
			this->mRazoulData->razoul->AddVx(-acceleratorX);

			if (this->mRazoulData->razoul->GetVx() < -20.0f)
			{
				this->mRazoulData->razoul->SetVx(-20.0f);
			}
		}
		elapsedTime += dt;
	}
}

void RazoulWalkOnFireState::HandleKeyboard(std::map<int, bool> keys)
{

}

void RazoulWalkOnFireState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

RazoulWalkOnFireState::StateName RazoulWalkOnFireState::GetState()
{
	return RazoulState::WalkOnFire;
}

RazoulWalkOnFireState::~RazoulWalkOnFireState()
{
}