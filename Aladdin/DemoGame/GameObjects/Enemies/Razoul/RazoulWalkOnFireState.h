#pragma once
#include "RazoulState.h"
#include "Razoul.h"
class RazoulWalkOnFireState : public RazoulState
{
public:
	RazoulWalkOnFireState(RazoulData *razoulData);
	~RazoulWalkOnFireState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	//do bien tien van toc sau moi frame tinh bang pixel / s
	float acceleratorX;
	float elapsedTime = 0.0f;
};

