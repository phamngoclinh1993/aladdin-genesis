#pragma once
#include "RazoulData.h"
#include "..\..\GameObject.h"
#include <map>

class RazoulState
{
public:
	enum StateName
	{
		// Running
		Running,
		// Attack
		Attack,
		WasAttacked,
		// Die
		Die,
		//stand
		Standing,
		// Walk on fire
		WalkOnFire
	};
	virtual void Update(float dt);

	virtual void HandleKeyboard(std::map<int, bool> keys);

	//side va cham voi player
	virtual void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	//ham thuan ao bat buoc phai ke thua
	virtual StateName GetState() = 0;
	~RazoulState();
protected:
	RazoulState();
	RazoulState(RazoulData *razoulData);

	RazoulData *mRazoulData;

};

