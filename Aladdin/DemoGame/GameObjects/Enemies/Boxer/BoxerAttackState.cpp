#include "BoxerAttackState.h"


BoxerAttackState::BoxerAttackState(BoxerData *boxerData)
{
	this->mBoxerData = boxerData;
}
BoxerAttackState::~BoxerAttackState()
{
}


void BoxerAttackState::Update(float dt)
{

}

void BoxerAttackState::HandleKeyboard(std::map<int, bool> keys)
{

}

void BoxerAttackState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

BoxerAttackState::StateName BoxerAttackState::GetState()
{
	return BoxerState::Attack;
}