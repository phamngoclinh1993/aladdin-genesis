#pragma once
#include "Boxer.h"
#include "BoxerData.h"
class BoxerStandingState : public BoxerState
{
public:
	BoxerStandingState(BoxerData *boxerData);
	~BoxerStandingState();
	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();
};

