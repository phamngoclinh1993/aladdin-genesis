#include "BoxerRunningState.h"
#include "BoxerAttackState.h"


BoxerRunningState::BoxerRunningState(BoxerData *boxerData)
{
	this->mBoxerData = boxerData;
	acceleratorX = 5.0f;
}


void BoxerRunningState::Update(float dt)
{

	if (elapsedTime > 5.0f)
	{
		this->mBoxerData->boxer->SetVx(0);
		this->mBoxerData->boxer->SetState(new BoxerAttackState(this->mBoxerData));
		return;
	}
	else
	{
		//di chuyen sang trai
		mBoxerData->boxer->SetReverse(true);
		if (this->mBoxerData->boxer->GetVx() > -20.0f)
		{
			this->mBoxerData->boxer->AddVx(-acceleratorX);

			if (this->mBoxerData->boxer->GetVx() < -20.0f)
			{
				this->mBoxerData->boxer->SetVx(-20.0f);
			}
		}
		elapsedTime += dt;
	}
}

void BoxerRunningState::HandleKeyboard(std::map<int, bool> keys)
{

}

void BoxerRunningState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

BoxerRunningState::StateName BoxerRunningState::GetState()
{
	return BoxerState::Running;
}

BoxerRunningState::~BoxerRunningState()
{
}
