#pragma once
#include "..\..\..\Components\Animation.h"
#include "..\..\GameObject.h"
#include "BoxerData.h"
#include "BoxerState.h"
class Boxer : public GameObject
{
public:
	Boxer();

	Boxer(D3DXVECTOR3 position);

	~Boxer();
	bool init(D3DXVECTOR3 position);

	void Update(float dt);

	void Draw(D3DXVECTOR3 position = D3DXVECTOR3(), RECT sourceRect = RECT(), D3DXVECTOR2 scale = D3DXVECTOR2(), D3DXVECTOR2 transform = D3DXVECTOR2(), float angle = 0, D3DXVECTOR2 rotationCenter = D3DXVECTOR2(), D3DXCOLOR colorKey = D3DCOLOR_XRGB(255, 255, 255));

	void Draw(D3DXVECTOR2 transform);

	void SetState(BoxerState *newState);

	void SetReverse(bool flag);

	BoxerState::StateName Boxer::getState();

protected:

	BoxerData *mBoxerData;

	Animation	*mCurrentAnimation,
		*mAnimationRunning,
		*mAnimationAttack,
		*mAnimationStanding,
		*mAnimationWasAttacked,
		*mAnimationWasOnFire;

	bool mCurrentReverse;
	float elapsedTime = 0.0f;

	void changeAnimation(BoxerState::StateName state);

	BoxerState::StateName mCurrentState;

	//do bien tien van toc sau moi frame tinh bang pixel / s
	float acceleratorX;
};

