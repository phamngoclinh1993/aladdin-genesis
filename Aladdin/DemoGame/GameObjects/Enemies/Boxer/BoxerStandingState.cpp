#include "BoxerStandingState.h"


BoxerStandingState::BoxerStandingState(BoxerData *boxerData)
{
	this->mBoxerData = boxerData;
}


void BoxerStandingState::Update(float dt)
{
}

void BoxerStandingState::HandleKeyboard(std::map<int, bool> keys)
{

}

void BoxerStandingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

BoxerStandingState::StateName BoxerStandingState::GetState()
{
	return BoxerState::Standing;
}

BoxerStandingState::~BoxerStandingState()
{

}