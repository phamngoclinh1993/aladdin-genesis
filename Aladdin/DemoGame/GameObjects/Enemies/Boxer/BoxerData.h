#pragma once
class Boxer;
class BoxerState;

class BoxerData
{
public:
	BoxerData();
	~BoxerData();

	Boxer		*boxer;
	BoxerState	*state;
};

