#include "Boxer.h"
#include "BoxerAttackState.h"
#include "BoxerRunningState.h"
#include "BoxerStandingState.h"

Boxer::Boxer()
{
}

Boxer::Boxer(D3DXVECTOR3 position)
{
	init(position);
}


Boxer::~Boxer()
{
}

bool Boxer::init(D3DXVECTOR3 position)
{
	SetPosition(position);

	mAnimationRunning = new Animation("Resources/Enemies/Boxer/Boxer-running.png", 12, 1, 12, 0.25f);
	mAnimationAttack = new Animation("Resources/Enemies/Boxer/Boxer-attack.png", 4, 1, 4, 0.25f);
	mAnimationWasAttacked = new Animation("Resources/Enemies/Boxer/Boxer-running.png", 12, 1, 12, 0.25f);
	mAnimationStanding = new Animation("Resources/Enemies/Boxer/Boxer-running.png", 12, 1, 12, 0.25f);
	mAnimationWasOnFire = new Animation("Resources/Enemies/Boxer/Boxer-running.png", 12, 1, 12, 0.25f);

	this->mBoxerData = new BoxerData();
	this->mBoxerData->boxer = this;
	this->vx = 0;
	this->vy = 0;
	this->SetState(new BoxerStandingState(this->mBoxerData));

	return true;
}


void Boxer::Update(float dt)
{
	mCurrentAnimation->Update(dt);

	if (this->mBoxerData->state)
	{
		this->mBoxerData->state->Update(dt);
	}

	GameObject::Update(dt);
}

void Boxer::Draw(D3DXVECTOR3 position, RECT sourceRect, D3DXVECTOR2 scale, D3DXVECTOR2 transform, float angle, D3DXVECTOR2 rotationCenter, D3DXCOLOR colorKey)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR3(posX, posY, 0));
}

void Boxer::Draw(D3DXVECTOR2 transform)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR2(transform));
}

void Boxer::SetReverse(bool flag)
{
	mCurrentReverse = flag;
}


void Boxer::SetState(BoxerState *newState)
{
	delete this->mBoxerData->state;

	this->mBoxerData->state = newState;

	this->changeAnimation(newState->GetState());

	mCurrentState = newState->GetState();
}


void Boxer::changeAnimation(BoxerState::StateName state)
{
	switch (state)
	{
	case BoxerState::Running:
		mCurrentAnimation = mAnimationRunning;
		break;

	case BoxerState::Attack:
		mCurrentAnimation = mAnimationAttack;
		break;

	case BoxerState::WasAttacked:
		mCurrentAnimation = mAnimationWasAttacked;
		break;
	case BoxerState::Standing:
		mCurrentAnimation = mAnimationStanding;
		break;

	case BoxerState::WalkOnFire:
		mCurrentAnimation = mAnimationWasOnFire;
		break;
	}

	this->width = mCurrentAnimation->GetWidth();
	this->height = mCurrentAnimation->GetHeight();
}



BoxerState::StateName Boxer::getState()
{
	return mCurrentState;
}