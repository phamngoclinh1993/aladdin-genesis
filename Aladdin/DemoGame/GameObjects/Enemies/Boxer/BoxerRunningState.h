#pragma once
#include "Boxer.h"
#include "BoxerState.h"
class BoxerRunningState :public BoxerState
{
public:
	BoxerRunningState(BoxerData *boxerData);
	~BoxerRunningState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	//do bien tien van toc sau moi frame tinh bang pixel / s
	float acceleratorX;
	float elapsedTime = 0.0f;
};

