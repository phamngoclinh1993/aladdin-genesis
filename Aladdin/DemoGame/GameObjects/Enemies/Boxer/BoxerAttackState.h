#pragma once
#include "Boxer.h"
#include "BoxerState.h"
class BoxerAttackState : public BoxerState
{
public:
	BoxerAttackState(BoxerData *boxerData);
	~BoxerAttackState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();
};

