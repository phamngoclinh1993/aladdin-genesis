#include "HandNormalState.h"
#include "HandFightingState.h"


HandNormalState::HandNormalState(HandData *HandData)
{
	this->mHandData = HandData;
}


HandNormalState::~HandNormalState()
{
}

void HandNormalState::Update(float dt)
{
	if (this->mHandData->hand->isFighting)
	{
		this->mHandData->hand->SetState(new HandFightingState(this->mHandData));
		return;
	}
}

void HandNormalState::HandleKeyboard(std::map<int, bool> keys)
{

}

void HandNormalState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

HandNormalState::StateName HandNormalState::GetState()
{
	return HandState::Normal;
}