#pragma once
#include "HandState.h"
#include "Hand.h"

class HandFightingState : public HandState
{
public:
	HandFightingState(HandData *HandData);
	~HandFightingState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	float ellapsedtime;
};
