#include "Hand.h"
#include "HandNormalState.h"
#include "HandFightingState.h"

Hand::Hand()
{
	isFighting = false;
}

Hand::Hand(D3DXVECTOR3 position)
{
	init(position);
}

Hand::~Hand()
{
}

bool Hand::init(D3DXVECTOR3 position)
{
	SetPosition(position);

	mAnimationNormal = new Animation("Resources/Enemies/Hand/hand-normal.png", 1, 1, 1, 0.15f);
	mAnimationFighting = new Animation("Resources/Enemies/Hand/hand-attack.png", 6, 1, 6, 0.15f);

	this->mHandData = new HandData();
	this->mHandData->hand = this;
	this->vx = 0;
	this->vy = 0;
	this->SetState(new HandNormalState(this->mHandData));

	return true;
}


void Hand::Update(float dt)
{
	mCurrentAnimation->Update(dt);

	if (this->mHandData->state)
	{
		this->mHandData->state->Update(dt);
	}

	GameObject::Update(dt);
}

void Hand::Draw(D3DXVECTOR3 position, RECT sourceRect, D3DXVECTOR2 scale, D3DXVECTOR2 transform, float angle, D3DXVECTOR2 rotationCenter, D3DXCOLOR colorKey)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR3(posX, posY, 0));
}

void Hand::Draw(D3DXVECTOR2 transform)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR2(transform));
}


void Hand::SetReverse(bool flag)
{
	mCurrentReverse = flag;
}


void Hand::SetState(HandState *newState)
{
	delete this->mHandData->state;

	this->mHandData->state = newState;

	this->changeAnimation(newState->GetState());

	mCurrentState = newState->GetState();
}


void Hand::changeAnimation(HandState::StateName state)
{
	switch (state)
	{
	case HandState::Normal:
		mCurrentAnimation = mAnimationNormal;
		break;

	case HandState::Fighting:
		mCurrentAnimation = mAnimationFighting;
		break;
	}

	this->width = mCurrentAnimation->GetWidth();
	this->height = mCurrentAnimation->GetHeight();
}



HandState::StateName Hand::getState()
{
	return mCurrentState;
}