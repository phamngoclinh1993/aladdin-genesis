#pragma once
class Hand;
class HandState;

class HandData
{
public:
	HandData();
	~HandData();

	Hand      *hand;
	HandState *state;
};

