#pragma once
#include "HandState.h"
#include "Hand.h"

class HandNormalState : public HandState
{
public:
	HandNormalState(HandData *HandData);
	~HandNormalState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();
};
