#include "HandFightingState.h"
#include "HandNormalState.h"


HandFightingState::HandFightingState(HandData *HandData)
{
	this->mHandData = HandData;
	ellapsedtime = 0.0f;
}


HandFightingState::~HandFightingState()
{
}

void HandFightingState::Update(float dt)
{
	ellapsedtime += dt;
	if (ellapsedtime > 1.0f)
	{
		this->mHandData->hand->isFighting = false;
		this->mHandData->hand->SetState(new HandNormalState(this->mHandData));
		return;
	}
}

void HandFightingState::HandleKeyboard(std::map<int, bool> keys)
{

}

void HandFightingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

HandFightingState::StateName HandFightingState::GetState()
{
	return HandState::Fighting;
}