#include "CamelFightingState.h"
#include "CamelNormalState.h"


CamelFightingState::CamelFightingState(CamelData *CamelData)
{
	this->mCamelData = CamelData;
	ellapsedtime = 0.0f;
}


CamelFightingState::~CamelFightingState()
{
}

void CamelFightingState::Update(float dt)
{
	ellapsedtime += dt;
	if (ellapsedtime > 1.0f)
	{
		this->mCamelData->camel->isFighting = false;
		this->mCamelData->camel->SetState(new CamelNormalState(this->mCamelData));
		return;
	}
}

void CamelFightingState::HandleKeyboard(std::map<int, bool> keys)
{

}

void CamelFightingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

CamelFightingState::StateName CamelFightingState::GetState()
{
	return CamelState::Fighting;
}