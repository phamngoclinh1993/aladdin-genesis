#pragma once
#include "CamelState.h"
#include "Camel.h"

class CamelNormalState : public CamelState
{
public:
	CamelNormalState(CamelData *CamelData);
	~CamelNormalState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();
};
