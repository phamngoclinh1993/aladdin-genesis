#pragma once
class CamelState;
class Camel;

class CamelData
{
public:
	CamelData();
	~CamelData();

	Camel      *camel;
	CamelState *state;
};
