#pragma once
#include "..\..\..\Components\Animation.h"
#include "..\..\GameObject.h"
#include "CamelData.h"
#include "CamelState.h"

class Camel : public GameObject
{
public:
	Camel();

	Camel(D3DXVECTOR3 position);

	~Camel();

	bool init(D3DXVECTOR3 position);

	void Update(float dt);

	void Draw(D3DXVECTOR3 position = D3DXVECTOR3(), RECT sourceRect = RECT(), D3DXVECTOR2 scale = D3DXVECTOR2(), D3DXVECTOR2 transform = D3DXVECTOR2(), float angle = 0, D3DXVECTOR2 rotationCenter = D3DXVECTOR2(), D3DXCOLOR colorKey = D3DCOLOR_XRGB(255, 255, 255));

	void Draw(D3DXVECTOR2 transform);

	void SetState(CamelState *newState);

	void SetReverse(bool flag);

	CamelState::StateName Camel::getState();

	bool isFighting;

protected:

	CamelData	*mCamelData;

	Animation	*mCurrentAnimation,
				*mAnimationNormal,
				*mAnimationFighting;

	bool mCurrentReverse;
	float elapsedTime = 0.0f;

	void changeAnimation(CamelState::StateName state);

	CamelState::StateName mCurrentState;
};

