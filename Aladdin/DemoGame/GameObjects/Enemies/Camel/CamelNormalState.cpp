#include "CamelNormalState.h"
#include "CamelFightingState.h"


CamelNormalState::CamelNormalState(CamelData *CamelData)
{
	this->mCamelData = CamelData;
}


CamelNormalState::~CamelNormalState()
{
}

void CamelNormalState::Update(float dt)
{
	if (this->mCamelData->camel->isFighting)
	{
		this->mCamelData->camel->SetState(new CamelFightingState(this->mCamelData));
		return;
	}
}

void CamelNormalState::HandleKeyboard(std::map<int, bool> keys)
{

}

void CamelNormalState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

CamelNormalState::StateName CamelNormalState::GetState()
{
	return CamelState::Normal;
}