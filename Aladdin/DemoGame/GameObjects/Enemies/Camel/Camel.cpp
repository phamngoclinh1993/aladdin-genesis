#include "Camel.h"
#include "CamelNormalState.h"
#include "CamelFightingState.h"

Camel::Camel()
{
	isFighting = false;
}

Camel::Camel(D3DXVECTOR3 position)
{
	init(position);
}

Camel::~Camel()
{
}

bool Camel::init(D3DXVECTOR3 position)
{
	SetPosition(position);

	mAnimationNormal = new Animation("Resources/Enemies/Camel/camel-normal.png", 1, 1, 1, 0.15f);
	mAnimationFighting = new Animation("Resources/Enemies/Camel/camel-attack.png", 6, 1, 6, 0.15f);

	this->mCamelData = new CamelData();
	this->mCamelData->camel = this;
	this->vx = 0;
	this->vy = 0;
	this->SetState(new CamelNormalState(this->mCamelData));

	return true;
}


void Camel::Update(float dt)
{
	mCurrentAnimation->Update(dt);

	if (this->mCamelData->state)
	{
		this->mCamelData->state->Update(dt);
	}

	GameObject::Update(dt);
}

void Camel::Draw(D3DXVECTOR3 position, RECT sourceRect, D3DXVECTOR2 scale, D3DXVECTOR2 transform, float angle, D3DXVECTOR2 rotationCenter, D3DXCOLOR colorKey)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR3(posX, posY, 0));
}

void Camel::Draw(D3DXVECTOR2 transform)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR2(transform));
}


void Camel::SetReverse(bool flag)
{
	mCurrentReverse = flag;
}


void Camel::SetState(CamelState *newState)
{
	delete this->mCamelData->state;

	this->mCamelData->state = newState;

	this->changeAnimation(newState->GetState());

	mCurrentState = newState->GetState();
}


void Camel::changeAnimation(CamelState::StateName state)
{
	switch (state)
	{
	case CamelState::Normal:
		mCurrentAnimation = mAnimationNormal;
		break;

	case CamelState::Fighting:
		mCurrentAnimation = mAnimationFighting;
		break;
	}

	this->width = mCurrentAnimation->GetWidth();
	this->height = mCurrentAnimation->GetHeight();
}



CamelState::StateName Camel::getState()
{
	return mCurrentState;
}