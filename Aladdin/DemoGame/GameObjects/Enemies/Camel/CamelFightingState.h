#pragma once
#include "CamelState.h"
#include "Camel.h"

class CamelFightingState : public CamelState
{
public:
	CamelFightingState(CamelData *CamelData);
	~CamelFightingState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	float ellapsedtime;
};
