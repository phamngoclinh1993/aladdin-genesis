#pragma once

#include "CamelData.h"
#include "..\..\GameObject.h"
#include <map>

class CamelState
{
public:
	~CamelState();

	enum StateName
	{
		Normal,
		Fighting
	};

	virtual void Update(float dt);

	virtual void HandleKeyboard(std::map<int, bool> keys);

	//side va cham voi player
	virtual void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	//ham thuan ao bat buoc phai ke thua
	virtual StateName GetState() = 0;

protected:
	CamelState(CamelData *CamelData);
	CamelState();

	CamelData *mCamelData;
};
