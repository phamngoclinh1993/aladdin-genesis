#pragma once
class HakimState;
class Hakim;

class HakimData
{
public:
	HakimData();
	~HakimData();

	Hakim      *hakim;
	HakimState *state;
};
