#include "HakimRunningState.h"
#include "HakimAttackState.h"
#include "HakimStandingState.h"
#include "..\..\..\Components\GameLog.h"

HakimRunningState::HakimRunningState(HakimData *hakimData)
{
	this->mHakimData = hakimData;
	acceleratorX = 10.0f;
}


HakimRunningState::~HakimRunningState()
{
}

void HakimRunningState::Update(float dt)
{
	//khoang cach giua vi tri hien tai so voi vi tri cua camera
	float temp = this->mHakimData->hakim->GetPosition().x - this->mHakimData->hakim->GetCamera()->GetPosition().x;

	//khoang cach giua vi tri khoi tao so voi vi tri hien tai cua enemy
	float tempA = this->mHakimData->hakim->getInitPosition().x - this->mHakimData->hakim->GetPosition().x;

	GAMELOG("%f %f", temp, tempA);

	if (abs(tempA) > 100.0f && (temp * tempA) > 0)
	{
		this->mHakimData->hakim->SetVx(0);
		this->mHakimData->hakim->SetState(new HakimStandingState(this->mHakimData));
		return;
	}
	else
	{
		if (temp > 0.0f)
		{
			//di chuyen sang trai
			if (this->mHakimData->hakim->GetVx() > -20.0f)
			{
				mHakimData->hakim->SetReverse(false);
				this->mHakimData->hakim->AddVx(-acceleratorX);

				if (this->mHakimData->hakim->GetVx() < -20.0f)
				{
					this->mHakimData->hakim->SetVx(-20.0f);
				}
			}
			if (abs(temp) < 30.0f)
			{
				this->mHakimData->hakim->SetVx(0);
				this->mHakimData->hakim->SetState(new HakimAttackState(this->mHakimData));
			}
		}
		else
		{
				this->mHakimData->hakim->SetReverse(true);
				this->mHakimData->hakim->SetVx(0);
				//di chuyen sang phai
				if (this->mHakimData->hakim->GetVx() < 20.0f)
				{
					this->mHakimData->hakim->AddVx(acceleratorX);

					if (this->mHakimData->hakim->GetVx() >= 50.0f)
					{
						this->mHakimData->hakim->SetVx(50.0f);
					}
				}
				if (abs(temp) < 30.0f)
				{
					this->mHakimData->hakim->SetVx(0);
					this->mHakimData->hakim->SetState(new HakimAttackState(this->mHakimData));
				}
		}

		elapsedTime += dt;
	}
}

void HakimRunningState::HandleKeyboard(std::map<int, bool> keys)
{

}

void HakimRunningState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	
}

HakimRunningState::StateName HakimRunningState::GetState()
{
	return HakimState::Running;
}