#pragma once
#include "HakimState.h"
#include "Hakim.h"

class HakimWasAttackedState : public HakimState
{
public:
	HakimWasAttackedState(HakimData *hakimData);
	~HakimWasAttackedState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	//do bien tien van toc sau moi frame tinh bang pixel / s
	float acceleratorX;
};