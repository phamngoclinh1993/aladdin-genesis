#include "Hakim.h"
#include "HakimRunningState.h"
#include "HakimAttackState.h"
#include "HakimWasAttackedState.h"
#include "HakimStandingState.h"
#include "..\..\..\Components\GameLog.h"

Hakim::Hakim()
{
}

Hakim::Hakim(D3DXVECTOR3 position)
{
	init(position);
}

Hakim::~Hakim()
{
}

bool Hakim::init(D3DXVECTOR3 position)
{
	SetPosition(position);

	

	mAnimationStanding = new Animation("Resources/Enemies/Hakim/hakim-standing.png", 1, 1, 1, 0.15f);
	mAnimationRunning = new Animation("Resources/Enemies/Hakim/hakim-running.png", 8, 1, 8, 0.15f);
	mAnimationAttack = new Animation("Resources/Enemies/Hakim/hakim-attack.png", 6, 1, 6, 0.15f);
	mAnimationWasAttacked = new Animation("Resources/Enemies/Hakim/hakim-was-attack.png", 9, 1, 9, 0.15f);

	this->mHakimData = new HakimData();
	this->mHakimData->hakim = this;
	this->vx = 0;
	this->vy = 0;

	this->initPosition = this->GetPosition();

	this->SetState(new HakimStandingState(this->mHakimData));

	return true;
}


void Hakim::Update(float dt)
{
	mCurrentAnimation->Update(dt);

	if (this->mHakimData->state)
	{
		this->mHakimData->state->Update(dt);
	}

	GameObject::Update(dt);
}

void Hakim::Draw(D3DXVECTOR3 position, RECT sourceRect, D3DXVECTOR2 scale, D3DXVECTOR2 transform, float angle, D3DXVECTOR2 rotationCenter, D3DXCOLOR colorKey)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR3(posX, posY, 0));
}

void Hakim::Draw(D3DXVECTOR2 transform)
{
	mCurrentAnimation->SetPosition(this->GetPosition());
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->Draw(D3DXVECTOR2(transform));
}


void Hakim::SetReverse(bool flag)
{
	mCurrentReverse = flag;
}


void Hakim::SetState(HakimState *newState)
{
	delete this->mHakimData->state;

	this->mHakimData->state = newState;

	this->changeAnimation(newState->GetState());

	mCurrentState = newState->GetState();
}


void Hakim::changeAnimation(HakimState::StateName state)
{
	switch (state)
	{
	case HakimState::Standing:
		mCurrentAnimation = mAnimationStanding;
		break;
	case HakimState::Running:
		mCurrentAnimation = mAnimationRunning;
		break;

	case HakimState::Attack:
		mCurrentAnimation = mAnimationAttack;
		break;

	case HakimState::WasAttacked:
		mCurrentAnimation = mAnimationWasAttacked;
		break;
	}

	this->width = mCurrentAnimation->GetWidth();
	this->height = mCurrentAnimation->GetHeight();
}

void Hakim::OnCollision(GameObject *impactor, GameObject::CollisionReturn data, GameObject::SideCollisions side)
{
	
	if (impactor->Tag == GameObject::ObjectTypes::Aladdin)
	{
		this->mHakimData->state->OnCollision(impactor, side, data);

		float tam = this->mHakimData->hakim->GetPosition().x - this->mHakimData->hakim->GetCamera()->GetPosition().x;
		if (abs(tam) < 30.0f && impactor->getWasAttacked())
		{
			this->mHakimData->hakim->SetVx(0);
			this->mHakimData->hakim->SetState(new HakimWasAttackedState(this->mHakimData));
		}
	}
}

HakimState::StateName Hakim::getState()
{
	return mCurrentState;
}

D3DXVECTOR3 Hakim::getInitPosition(){
	return this->initPosition;
}