#include "HakimStandingState.h"
#include "..\..\..\Components\GameLog.h"
#include "HakimRunningState.h"
#include "HakimAttackState.h"
#include "..\..\..\Components\GameVariables.h"

HakimStandingState::HakimStandingState(HakimData *hakimData)
{
	this->mHakimData = hakimData;
}


HakimStandingState::~HakimStandingState()
{
}

void HakimStandingState::Update(float dt)
{
	if (this->mHakimData->hakim->GetCamera() != NULL)
	{
		//khoang cach giua vi tri hien tai so voi vi tri cua camera
		float temp = this->mHakimData->hakim->GetPosition().x - this->mHakimData->hakim->GetCamera()->GetPosition().x;

		//khoang cach giua vi tri khoi tao so voi vi tri hien tai cua enemy
		float tempA = this->mHakimData->hakim->getInitPosition().x - this->mHakimData->hakim->GetPosition().x;


		if (abs(tempA) > 100.0f && (temp * tempA) >0 )
		{
			if (abs(temp) < 30.0f)
			{
				this->mHakimData->hakim->SetVx(0);
				this->mHakimData->hakim->SetState(new HakimAttackState(this->mHakimData));
			}
			return;
		}

		if (temp < 200.0f && temp > 10.0f)
		{
			this->mHakimData->hakim->SetVx(0);
			this->mHakimData->hakim->SetState(new HakimRunningState(this->mHakimData));
			return;
		}
		else
		{
			if (temp > -200.0f / 2 && temp < -10.0f)
			{
				this->mHakimData->hakim->SetVx(0);
				this->mHakimData->hakim->SetReverse(true);
				this->mHakimData->hakim->SetState(new HakimRunningState(this->mHakimData));
			}
		}
	
	}
}

void HakimStandingState::HandleKeyboard(std::map<int, bool> keys)
{

}

void HakimStandingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	//GAMELOG("++++++ HAKIM STANDING COLLISION ++++++");
}

HakimStandingState::StateName HakimStandingState::GetState()
{
	return HakimState::Standing;
}
