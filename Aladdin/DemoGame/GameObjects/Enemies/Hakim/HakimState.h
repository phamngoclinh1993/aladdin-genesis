#pragma once

#include "HakimData.h"
#include "..\..\GameObject.h"
#include <map>

class HakimState
{
public:
	~HakimState();

	enum StateName
	{
		// Running
		Standing,
		Running,
		RunningJump,

		// Attack
		Attack,
		WasAttacked,

		// Die
		Die
	};

	virtual void Update(float dt);

	virtual void HandleKeyboard(std::map<int, bool> keys);

	//side va cham voi player
	virtual void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	//ham thuan ao bat buoc phai ke thua
	virtual StateName GetState() = 0;

protected:
	HakimState(HakimData *hakimData);
	HakimState();

	HakimData *mHakimData;
};
