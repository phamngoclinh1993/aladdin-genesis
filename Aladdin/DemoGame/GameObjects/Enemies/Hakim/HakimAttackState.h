#pragma once
#include "HakimState.h"
#include "Hakim.h"

class HakimAttackState : public HakimState
{
public:
	HakimAttackState(HakimData *hakimData);
	~HakimAttackState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	//do bien tien van toc sau moi frame tinh bang pixel / s
	float acceleratorX;
};
