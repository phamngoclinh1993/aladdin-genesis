#include "HakimAttackState.h"
#include "HakimRunningState.h"

HakimAttackState::HakimAttackState(HakimData *hakimData)
{
	this->mHakimData = hakimData;
}


HakimAttackState::~HakimAttackState()
{
}

void HakimAttackState::Update(float dt)
{
	float temp = this->mHakimData->hakim->GetPosition().x - this->mHakimData->hakim->GetCamera()->GetPosition().x;
	if (abs(temp) > 30.0f)
	{
		this->mHakimData->hakim->SetVx(0);
		this->mHakimData->hakim->SetState(new HakimRunningState(this->mHakimData));
	}
}

void HakimAttackState::HandleKeyboard(std::map<int, bool> keys)
{

}

void HakimAttackState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	
}

HakimAttackState::StateName HakimAttackState::GetState()
{
	return HakimState::Attack;
}