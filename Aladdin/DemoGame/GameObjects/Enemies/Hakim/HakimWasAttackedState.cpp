#include "HakimWasAttackedState.h"
#include "HakimAttackState.h"

HakimWasAttackedState::HakimWasAttackedState(HakimData *hakimData)
{
	this->mHakimData = hakimData;
}


HakimWasAttackedState::~HakimWasAttackedState()
{
}

void HakimWasAttackedState::Update(float dt)
{

}

void HakimWasAttackedState::HandleKeyboard(std::map<int, bool> keys)
{

}

void HakimWasAttackedState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	if (impactor->Tag == GameObject::ObjectTypes::Aladdin)
	{
		float tam = this->mHakimData->hakim->GetPosition().x - this->mHakimData->hakim->GetCamera()->GetPosition().x;
		if (abs(tam) < 30.0f && !impactor->getWasAttacked())
		{
			this->mHakimData->hakim->SetVx(0);
			this->mHakimData->hakim->SetState(new HakimAttackState(this->mHakimData));
		}
	}
}

HakimWasAttackedState::StateName HakimWasAttackedState::GetState()
{
	return HakimState::WasAttacked;
}