#include "AladdinRunningJumpState.h"
#include "AladdinStandingState.h"
#include "AladdinRunningState.h"
#include "AladdinRunningFallingState.h"

AladdinRunningJumpState::AladdinRunningJumpState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
	this->mAladdinData->aladdin->SetVy(-400.0f);

	acceleratorY = 15.0f;
	acceleratorX = 14.0f;

	noPressed = false;
}


AladdinRunningJumpState::~AladdinRunningJumpState()
{

}

void AladdinRunningJumpState::Update(float dt)
{
	// Example code

	this->mAladdinData->aladdin->AddVy(acceleratorY);

	if (mAladdinData->aladdin->GetVy() >= 0)
	{
		mAladdinData->aladdin->SetState(new AladdinRunningFallingState(this->mAladdinData));

		return;
	}

	if (noPressed)
	{
		if (mAladdinData->aladdin->getMoveDirection() == Aladdin::MoveToLeft)
		{
			//player dang di chuyen sang ben trai      
			if (mAladdinData->aladdin->GetVx() < 0)
			{
				this->mAladdinData->aladdin->AddVx(acceleratorX);

				if (mAladdinData->aladdin->GetVx() > 0)
					this->mAladdinData->aladdin->SetVx(0);
			}
		}
		else if (mAladdinData->aladdin->getMoveDirection() == Aladdin::MoveToRight)
		{
			//player dang di chuyen sang phai   
			if (mAladdinData->aladdin->GetVx() > 0)
			{
				this->mAladdinData->aladdin->AddVx(-acceleratorX);

				if (mAladdinData->aladdin->GetVx() < 0)
					this->mAladdinData->aladdin->SetVx(0);
			}
		}
	}
}

void AladdinRunningJumpState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_RIGHT])
	{
		mAladdinData->aladdin->SetReverse(false);

		//di chuyen sang phai
		if (this->mAladdinData->aladdin->GetVx() < 150.0f)
		{
			this->mAladdinData->aladdin->AddVx(acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() >= 150.0f)
			{
				this->mAladdinData->aladdin->SetVx(150.0f);
			}
		}

		noPressed = false;
	}
	else if (keys[VK_LEFT])
	{
		mAladdinData->aladdin->SetReverse(true);

		//di chuyen sang trai
		if (this->mAladdinData->aladdin->GetVx() > -150.0f)
		{
			this->mAladdinData->aladdin->AddVx(-acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() < -150.0f)
			{
				this->mAladdinData->aladdin->SetVx(-150.0f);
			}
		}

		noPressed = false;
	}
	else
	{
		noPressed = true;
	}
}

void AladdinRunningJumpState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinRunningJumpState::GetState()
{
	return AladdinState::RunningJump;
}
