#include "AladdinJumpingState.h"
#include "AladdinFallingState.h"

AladdinJumpingState::AladdinJumpingState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
	this->mAladdinData->aladdin->SetVy(-400.0f);
	
	acceleratorY = 15.0f;
	acceleratorX = 14.0f;

	noPressed = false;
}


AladdinJumpingState::~AladdinJumpingState()
{

}

void AladdinJumpingState::Update(float dt)
{
	// Example code
	
	this->mAladdinData->aladdin->AddVy(acceleratorY);

	if (mAladdinData->aladdin->GetVy() >= 0)
	{
		mAladdinData->aladdin->SetState(new AladdinFallingState(this->mAladdinData));

		return;
	}

	if (noPressed)
	{
		if (mAladdinData->aladdin->getMoveDirection() == Aladdin::MoveToLeft)
		{
			//player dang di chuyen sang ben trai      
			if (mAladdinData->aladdin->GetVx() < 0)
			{
				this->mAladdinData->aladdin->AddVx(acceleratorX);

				if (mAladdinData->aladdin->GetVx() > 0)
					this->mAladdinData->aladdin->SetVx(0);
			}
		}
		else if (mAladdinData->aladdin->getMoveDirection() == Aladdin::MoveToRight)
		{
			//player dang di chuyen sang phai   
			if (mAladdinData->aladdin->GetVx() > 0)
			{
				this->mAladdinData->aladdin->AddVx(-acceleratorX);

				if (mAladdinData->aladdin->GetVx() < 0)
					this->mAladdinData->aladdin->SetVx(0);
			}
		}
	}
}

void AladdinJumpingState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_RIGHT])
	{
		mAladdinData->aladdin->SetReverse(false);

		//di chuyen sang phai
		if (this->mAladdinData->aladdin->GetVx() < 150.0f)
		{
			this->mAladdinData->aladdin->AddVx(acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() >= 150.0f)
			{
				this->mAladdinData->aladdin->SetVx(150.0f);
			}
		}

		noPressed = false;
	}
	else if (keys[VK_LEFT])
	{
		mAladdinData->aladdin->SetReverse(true);

		//di chuyen sang trai
		if (this->mAladdinData->aladdin->GetVx() > -150.0f)
		{
			this->mAladdinData->aladdin->AddVx(-acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() < -150.0f)
			{
				this->mAladdinData->aladdin->SetVx(-150.0f);
			}
		}

		noPressed = false;
	}
	else
	{
		noPressed = true;
	}
}

void AladdinJumpingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	/*switch (side)
	{
	case GameObject::Left:
	{
						 this->mAladdinData->aladdin->AddPosition(data.RegionCollision.right - data.RegionCollision.left, 0);
						 this->mAladdinData->aladdin->SetVx(0);
						 break;
	}

	case GameObject::Right:
	{
						  this->mAladdinData->aladdin->AddPosition(-(data.RegionCollision.right - data.RegionCollision.left), 0);
						  this->mAladdinData->aladdin->SetVx(0);
						  break;
	}

	case GameObject::TopRight: case GameObject::TopLeft: case GameObject::Top:
	{
							   this->mAladdinData->aladdin->AddPosition(0, data.RegionCollision.bottom - data.RegionCollision.top);
							   this->mAladdinData->aladdin->SetVy(0);
							   break;
	}

	case GameObject::BottomRight: case GameObject::BottomLeft: case GameObject::Bottom:
	{
								  this->mAladdinData->aladdin->AddPosition(0, -(data.RegionCollision.bottom - data.RegionCollision.top));
	}

	default:
		break;
	}*/
}

AladdinState::StateName AladdinJumpingState::GetState()
{
	return AladdinState::Jumping;
}
