#include "AladdinClimbingLeftState.h"
#include "AladdinClimbingState.h"
#include "AladdinClimbingFightState.h"


AladdinClimbingLeftState::AladdinClimbingLeftState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinClimbingLeftState::~AladdinClimbingLeftState()
{
}

void AladdinClimbingLeftState::Update(float dt)
{

}

void AladdinClimbingLeftState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_LEFT])
	{
		mAladdinData->aladdin->SetReverse(false);
	}
	else if (keys[VK_RIGHT])
	{
		mAladdinData->aladdin->SetReverse(true);
	}
	// Example climbing state
	else if (keys[0x58])
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingFightState(this->mAladdinData));
		return;
	}
	else
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingState(this->mAladdinData));
		return;
	}
}

void AladdinClimbingLeftState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinClimbingLeftState::GetState()
{
	return AladdinState::ClimbingLeft;
}