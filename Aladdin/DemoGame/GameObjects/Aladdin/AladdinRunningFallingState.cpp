#include "AladdinRunningFallingState.h"
#include "AladdinRunningState.h"
#include "AladdinStandingState.h"
#include "..\..\Components\Collision.h"
#include "..\..\Components\GameLog.h"

AladdinRunningFallingState::AladdinRunningFallingState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;

	acceleratorY = 15.0f;
	acceleratorX = 8.0f;

	if (this->mAladdinData->aladdin->GetVx() == 0)
	{
		allowMoveX = false;
	}
	else
	{
		allowMoveX = true;
	}
}


AladdinRunningFallingState::~AladdinRunningFallingState()
{

}

void AladdinRunningFallingState::Update(float dt)
{
	this->mAladdinData->aladdin->AddVy(acceleratorY);

	if (mAladdinData->aladdin->GetVy() > 480.0f)
	{
		mAladdinData->aladdin->SetVy(480.0f);
	}
}

void AladdinRunningFallingState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_RIGHT])
	{
		mAladdinData->aladdin->SetReverse(false);

		isLeftOrRightKeyPressed = true;
		//di chuyen sang phai
		if (this->mAladdinData->aladdin->GetVx() < 150.0f)
		{
			this->mAladdinData->aladdin->AddVx(acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() >= 150.0f)
			{
				this->mAladdinData->aladdin->SetVx(150.0f);
			}
		}
	}
	else if (keys[VK_LEFT])
	{
		mAladdinData->aladdin->SetReverse(true);

		isLeftOrRightKeyPressed = true;
		//di chuyen sang trai
		if (this->mAladdinData->aladdin->GetVx() > -150.0f)
		{
			this->mAladdinData->aladdin->AddVx(-acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() <= -150.0f)
			{
				this->mAladdinData->aladdin->SetVx(-150.0f);
			}
		}
	}
	else
	{
		isLeftOrRightKeyPressed = false;
	}
}

void AladdinRunningFallingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	switch (side)
	{
	case GameObject::Left:
		//GAMELOG("RUNNING FALLING COLLISION LEFT");
		break;

	case GameObject::Right:
		//GAMELOG("RUNNING FALLING COLLISION RIGHT");
		break;

	case GameObject::Top:
		//GAMELOG("RUNNING FALLING COLLISION TOP");
		break;

	case GameObject::Bottom:
	case GameObject::BottomRight:
	case GameObject::BottomLeft:
		//GAMELOG("RUNNING FALLING COLLISION BOTTOM - BOTTOM RIGHT - BOTTOM LEFT");
		if (impactor->Tag == GameObject::ObjectTypes::Earth)
		{
			if (data.RegionCollision.right - data.RegionCollision.left >= 8.0f)
			{
				this->mAladdinData->aladdin->AddPosition(0, -(data.RegionCollision.bottom - data.RegionCollision.top));

				if (isLeftOrRightKeyPressed)
				{
					this->mAladdinData->aladdin->SetState(new AladdinRunningState(this->mAladdinData));
				}
				else
				{
					this->mAladdinData->aladdin->SetState(new AladdinStandingState(this->mAladdinData));
				}
			}
		}
		return;

	default:
		break;
	}
}

AladdinState::StateName AladdinRunningFallingState::GetState()
{
	return AladdinState::Falling;
}
