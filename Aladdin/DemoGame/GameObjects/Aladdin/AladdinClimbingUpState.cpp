#include "AladdinClimbingUpState.h"
#include "AladdinClimbingLeftState.h"


AladdinClimbingUpState::AladdinClimbingUpState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinClimbingUpState::~AladdinClimbingUpState()
{
}

void AladdinClimbingUpState::Update(float dt)
{

}

void AladdinClimbingUpState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_UP])
	{
		
	}
	else if (keys[VK_DOWN])
	{
		
	}
	// Example climbing state
	else if (keys[VK_LEFT] || keys[VK_RIGHT])
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingLeftState(this->mAladdinData));
		return;
	}
	else
	{
		
	}
}

void AladdinClimbingUpState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinClimbingUpState::GetState()
{
	return AladdinState::ClimbingUp;
}