#include "Aladdin.h"
#include "AladdinStandingState.h"
#include "AladdinFallingState.h"
#include "AladdinDieingState.h"
#include "..\..\Components\Collision.h"
#include "..\..\Components\GameLog.h"

Aladdin::Aladdin()
{
	mAnimationStanding = new Animation("Resources/Aladdin_Standing.png", 7, 1, 7, 0.3f);
	mAnimationStandingIdle2 = new Animation("Resources/Aladdin_Standing_Idle2.png", 15, 1, 15, 0.3f);
	mAnimationStandingIdle3 = new Animation("Resources/Aladdin_Standing_Idle3.png", 17, 1, 17, 0.3f);
	mAnimationStandingFight = new Animation("Resources/Aladdin_Standing_Fight.png", 5, 1, 5, 0.2f);
	mAnimationStandingFightWithApple = new Animation("Resources/Aladdin_Standing_Fight_With_Apple.png", 6, 1, 6, 0.2f);
	mAnimationJumping = new Animation("Resources/Aladdin_Jumping.png", 10, 1, 10, 0.3f);
	mAnimationRunning = new Animation("Resources/Aladdin_Running.png", 13, 1, 13, 0.2f);
	mAnimationRunningJump = new Animation("Resources/Aladdin_Running_Jump.png", 9, 1, 9, 0.3f);
	mAnimationSitting = new Animation("Resources/Aladdin_Sitting.png", 4, 1, 4, 0.2f);
	mAnimationSittingThrow = new Animation("Resources/Aladdin_Sitting_Apple_Shoot.png", 5, 1, 5, 0.2f);
	mAnimationSittingFight = new Animation("Resources/Aladdin_Sitting_Fight.png", 7, 1, 7, 0.2f);
	mAnimationClimbing = new Animation("Resources/Aladdin_Climbing.png", 5, 1, 5, 0.2f);
	mAnimationClimbingUp = new Animation("Resources/Aladdin_Climbing_Up.png", 10, 1, 10, 0.2f);
	mAnimationClimbingLeft = new Animation("Resources/Aladdin_Climbing_Left.png", 10, 1, 10, 0.2f);
	mAnimationClimbingFight = new Animation("Resources/Aladdin_Climbing_Fight.png", 7, 1, 7, 0.2f);
	mAnimationClimbingFightWithApple = new Animation("Resources/Aladdin_Climbing_Fight_With_Apple.png", 5, 1, 5, 0.2f);
	mAnimationPushing = new Animation("Resources/Aladdin_Pushing.png", 9, 1, 9, 0.2f);
	mAnimationDie = new Animation("Resources/Aladdin_Die.png", 13, 1, 13, 0.2f);

	this->mAladdinData = new AladdinData();
	this->mAladdinData->aladdin = this;
	this->vx = 0;
	this->vy = 0;
	this->SetState(new AladdinStandingState(this->mAladdinData));

	for (int i = 0; i < MAX_BULLET_APPLE; i++)
	{
		_Bullet[i] = NULL;
	}

	allowJump = true;
	heath = 7;
}

Aladdin::~Aladdin()
{
}

void Aladdin::UpdateBullet(float dt)
{	
	for (int i = 0; i < MAX_BULLET_APPLE; i++)
	{
		if (_Bullet[i] != NULL)
		{
			if (_Bullet[i]->_isDead == true 
				|| _Bullet[i]->GetPosition().x < 0
				|| _Bullet[i]->GetPosition().x > this->GetPosition().x + SCREEN_WIDTH / 2
				|| _Bullet[i]->GetPosition().x < this->GetPosition().x - SCREEN_WIDTH / 2
				|| _Bullet[i]->GetPosition().y < this->GetPosition().y - SCREEN_HEIGHT / 2
				|| _Bullet[i]->GetPosition().y > this->GetPosition().y + SCREEN_HEIGHT / 2)
			{
				_Bullet[i] = NULL;
				_nBullet -= 1;
			}
			else
			{
				_Bullet[i]->SetPosition(this->GetPosition());
				_Bullet[i]->Update(dt);
			}
		}
	}
}

void Aladdin::Update(float dt)
{
	if (heath > 0)
	{
		mCurrentAnimation->Update(dt);

		if (this->mAladdinData->state)
		{
			this->mAladdinData->state->Update(dt);
		}

		if (isHurt)
		{
			ellapsedTime += dt;
			if (ellapsedTime > 3.0)
			{
				isHurt = false;
				ellapsedTime = 0.0;
				GAMELOG("---- NO HURT ----");
			}
		}

		this->UpdateBullet(dt);

		GameObject::Update(dt);
	}
	// Aladdin die
	else
	{
		this->SetState(new AladdinDieingState(this->mAladdinData));

		//TODO
	}
}

void Aladdin::ThrowBullet()
{
	if (_nBullet < MAX_BULLET_APPLE && _totalApple > 0)
	{
		for (int i = 0; i < MAX_BULLET_APPLE; i++)
		{
			if (_Bullet[i] == NULL)
			{
				_Bullet[i] = new BulletApple(this->GetPosition());
				if (this->lastRightDirection) {
					_Bullet[i]->direction = 1;
				}
				else {
					_Bullet[i]->direction = -1;
				}
				_nBullet += 1;
				_totalApple -= 1;
				GAMELOG("--------------- TOTAL APPLE : %d ---------------", _totalApple);
				_throwBullet = true;
				_timeToThrow = 0;
				return;
			}
		}
	}
}

void Aladdin::HandleKeyboard(std::map<int, bool> keys)
{
	if (this->mAladdinData->state)
	{
		this->mAladdinData->state->HandleKeyboard(keys);
	}
}

void Aladdin::OnKeyPressed(int key)
{
	
}

void Aladdin::OnKeyUp(int key)
{
	
}

void Aladdin::SetReverse(bool flag)
{
	mCurrentReverse = flag;
}

void Aladdin::SetCamera(Camera *camera)
{
	this->mCamera = camera;
}

void Aladdin::DrawBullets(Camera *camera)
{
	for (int i = 0; i < MAX_BULLET_APPLE; i++)
	{
		if (_Bullet[i] != NULL && _Bullet[i]->_isDead == false)
		{
			if (camera != NULL) {
				_Bullet[i]->setCamera(this->mCamera);
			}
			_Bullet[i]->Draw();
		}
	}
}

void Aladdin::Draw(D3DXVECTOR3 position, RECT sourceRect, D3DXVECTOR2 scale, D3DXVECTOR2 transform, float angle, D3DXVECTOR2 rotationCenter, D3DXCOLOR colorKey)
{
	mCurrentAnimation->FlipVertical(mCurrentReverse);
	mCurrentAnimation->SetPosition(this->GetPosition());

	if (mCamera)
	{
		D3DXVECTOR2 trans = D3DXVECTOR2(GameGlobal::GetWidth() / 2 - mCamera->GetPosition().x,
			GameGlobal::GetHeight() / 2 - mCamera->GetPosition().y);

		mCurrentAnimation->Draw(D3DXVECTOR3(posX, posY, 0), sourceRect, scale, trans, angle, rotationCenter, colorKey);

		this->DrawBullets(mCamera);
	}
	else
	{
		mCurrentAnimation->Draw(D3DXVECTOR3(posX, posY, 0));
	}
}

void Aladdin::SetState(AladdinState *newState)
{
	allowMoveLeft = true;
	allowMoveRight = true;
	
	delete this->mAladdinData->state;

	this->mAladdinData->state = newState;

	this->changeAnimation(newState->GetState());

	mCurrentState = newState->GetState();
}

void Aladdin::OnCollision(GameObject *impactor, GameObject::CollisionReturn data, GameObject::SideCollisions side)
{
	if (!impactor->IsDead())
	{
		this->mAladdinData->state->OnCollision(impactor, side, data);

		if (impactor->Tag == GameObject::ObjectTypes::Apple)
		{
			_totalApple += 1;
			GAMELOG("--------------- TOTAL APPLE : %d ---------------", _totalApple);
			impactor->Dead();
			impactor = NULL;
		}
		else if (impactor->Tag == GameObject::ObjectTypes::Hakim
		|| impactor->Tag == GameObject::ObjectTypes::Hand
		|| impactor->Tag == GameObject::ObjectTypes::Knife
		|| impactor->Tag == GameObject::ObjectTypes::Boxer
		|| impactor->Tag == GameObject::ObjectTypes::Razoul)
		{
			if (!isHurt)
			{
				isHurt = true;
				this->GetAttack();
				GAMELOG("---- HURTING ----");
			}
		}
	}
	else
	{
		impactor->Dead();
		impactor = NULL;
	}
	
	
}

RECT Aladdin::GetBound()
{
	RECT rect;
	rect.left = this->posX - mCurrentAnimation->GetWidth() / 2;
	rect.right = rect.left + mCurrentAnimation->GetWidth();
	rect.top = this->posY - mCurrentAnimation->GetHeight() / 2;
	rect.bottom = rect.top + mCurrentAnimation->GetHeight();

	return rect;
}

void Aladdin::changeAnimation(AladdinState::StateName state)
{
	switch (state)
	{
	case AladdinState::Standing:
		mCurrentAnimation = mAnimationStanding;
		break;

	case AladdinState::StandingIdle2:
		mCurrentAnimation = mAnimationStandingIdle2;
		break;

	case AladdinState::StandingIdle3:
		mCurrentAnimation = mAnimationStandingIdle3;
		break;

	case AladdinState::StandingFight:
		mCurrentAnimation = mAnimationStandingFight;
		break;

	case AladdinState::StandingFightWithApple:
		mCurrentAnimation = mAnimationStandingFightWithApple;
		break;

	case AladdinState::Running:
		mCurrentAnimation = mAnimationRunning;
		break;

	case AladdinState::RunningJump:
		mCurrentAnimation = mAnimationRunningJump;
		break;

	case AladdinState::Jumping:
		mCurrentAnimation = mAnimationJumping;
		break;

	case AladdinState::Sitting:
		mCurrentAnimation = mAnimationSitting;
		break;

	case AladdinState::SittingFight:
		mCurrentAnimation = mAnimationSittingFight;
		break;

	case AladdinState::SittingFightWithApple:
		mCurrentAnimation = mAnimationSittingThrow;
		break;

	case AladdinState::Climbing:
		mCurrentAnimation = mAnimationClimbing;
		break;

	case AladdinState::ClimbingUp:
		mCurrentAnimation = mAnimationClimbingUp;
		break;

	case AladdinState::ClimbingLeft:
		mCurrentAnimation = mAnimationClimbingLeft;
		break;

	case AladdinState::ClimbingFight:
		mCurrentAnimation = mAnimationClimbingFight;
		break;

	case AladdinState::ClimbingFightWithApple:
		mCurrentAnimation = mAnimationClimbingFightWithApple;
		break;

	case AladdinState::Pushing:
		mCurrentAnimation = mAnimationPushing;
		break;

	case AladdinState::Die:
		mCurrentAnimation = mAnimationDie;
		break;
	}

	this->width = mCurrentAnimation->GetWidth();
	this->height = mCurrentAnimation->GetHeight();
}

Aladdin::MoveDirection Aladdin::getMoveDirection()
{
	if (this->vx > 0)
	{
		lastRightDirection = true;
		return MoveDirection::MoveToRight;
	}
	else if (this->vx < 0)
	{
		lastRightDirection = false;
		return MoveDirection::MoveToLeft;
	}

	return MoveDirection::None;
}

void Aladdin::OnNoCollisionWithBottom()
{
	if (mCurrentState != AladdinState::Jumping && mCurrentState != AladdinState::Falling)
	{
		this->SetState(new AladdinFallingState(this->mAladdinData));
	}
}

AladdinState::StateName Aladdin::getState()
{
	return mCurrentState;
}

Animation* Aladdin::getCurrentAnimation()
{
	return mCurrentAnimation;
}