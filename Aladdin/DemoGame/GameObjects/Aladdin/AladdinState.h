#pragma once

#include "AladdinData.h"
#include "..\GameObject.h"
#include <map>

class AladdinState
{
public:
	~AladdinState();

	enum StateName
	{
		//Standing
		Standing,
		StandingIdle2,
		StandingIdle3,
		StandingFight,
		StandingFightWithApple,

		// Running
		Running,
		RunningJump,

		// Jumping
		Jumping,

		// Falling
		Falling,

		// Sitting
		Sitting,
		SittingFight,
		SittingFightWithApple,

		// Climbing
		Climbing,
		ClimbingUp,
		ClimbingLeft,
		ClimbingFight,
		ClimbingFightWithApple,

		// Pushing
		Pushing,

		// Die
		Die
	};

	virtual void Update(float dt);

	virtual void HandleKeyboard(std::map<int, bool> keys);

	//side va cham voi player
	virtual void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	//ham thuan ao bat buoc phai ke thua
	virtual StateName GetState() = 0;

protected:
	AladdinState(AladdinData *aladdinData);
	AladdinState();

	AladdinData *mAladdinData;
};

