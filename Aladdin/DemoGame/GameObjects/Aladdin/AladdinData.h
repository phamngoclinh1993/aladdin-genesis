#pragma once
class AladdinState;
class Aladdin;

class AladdinData
{
public:
	AladdinData();
	~AladdinData();

	Aladdin      *aladdin;
	AladdinState *state;
};

