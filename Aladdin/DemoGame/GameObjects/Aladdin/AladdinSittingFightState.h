#pragma once

#include "AladdinState.h"
#include "Aladdin.h"

class AladdinSittingFightState : public AladdinState
{
public:
	AladdinSittingFightState(AladdinData *aladdinData);
	~AladdinSittingFightState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
};

