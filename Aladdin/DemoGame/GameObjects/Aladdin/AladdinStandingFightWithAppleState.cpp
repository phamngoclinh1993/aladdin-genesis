#include "AladdinStandingFightWithAppleState.h"
#include "AladdinStandingState.h"


AladdinStandingFightWithAppleState::AladdinStandingFightWithAppleState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinStandingFightWithAppleState::~AladdinStandingFightWithAppleState()
{
}

void AladdinStandingFightWithAppleState::Update(float dt)
{
	if (elapsedTime > 1.0f)
	{
		isThrow = true;
	}
	else
	{
		elapsedTime += dt;
	}
}

void AladdinStandingFightWithAppleState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[0x41] && isThrow)
	{
		// Fight with apple
		this->mAladdinData->aladdin->ThrowBullet();
		isThrow = false;
	}
	else if (elapsedTime > 0.35)
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingState(this->mAladdinData));
		return;
	}
}

void AladdinStandingFightWithAppleState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinStandingFightWithAppleState::GetState()
{
	return AladdinState::StandingFightWithApple;
}