#pragma once

#include "AladdinState.h"
#include "Aladdin.h"

class AladdinClimbingState : public AladdinState
{
public:
	AladdinClimbingState(AladdinData *aladdinData);
	~AladdinClimbingState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	int currentFrame;
};

