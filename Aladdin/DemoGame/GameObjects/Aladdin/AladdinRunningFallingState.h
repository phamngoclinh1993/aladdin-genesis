#pragma once
#include "Aladdin.h"
#include "AladdinState.h"

class AladdinRunningFallingState : public AladdinState
{
public:
	AladdinRunningFallingState(AladdinData *aladdinData);
	~AladdinRunningFallingState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	float acceleratorY;
	float acceleratorX;

	//neu nhu van toc ban dau = khong thi se khong cho giam toc do
	bool allowMoveX;

	bool isLeftOrRightKeyPressed;
};