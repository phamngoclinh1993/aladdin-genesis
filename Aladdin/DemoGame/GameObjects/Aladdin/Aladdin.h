#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include "..\..\Components\GameVariables.h"
#include "..\..\Components\Animation.h"
#include "..\..\Components\GameTime.h"
#include "..\..\Components\Camera.h"
#include "..\GameObject.h"
#include "AladdinData.h"
#include "AladdinState.h"
#include "..\..\Bullets\BulletApple.h"

class Aladdin : public GameObject
{
public:
	Aladdin();
	~Aladdin();

	enum MoveDirection
	{
		MoveToLeft, //chay tu phai sang trai
		MoveToRight, //chay tu trai sang phai
		None //dung im
	};

	void SetCamera(Camera *camera);

	void Update(float dt);

	void UpdateBullet(float dt);

	void Draw(D3DXVECTOR3 position = D3DXVECTOR3(), RECT sourceRect = RECT(), D3DXVECTOR2 scale = D3DXVECTOR2(), D3DXVECTOR2 transform = D3DXVECTOR2(), float angle = 0, D3DXVECTOR2 rotationCenter = D3DXVECTOR2(), D3DXCOLOR colorKey = D3DCOLOR_XRGB(255, 255, 255));

	void DrawBullets(Camera *camera);

	void ThrowBullet();

	void SetState(AladdinState *newState);

	void OnCollision(GameObject *impactor, GameObject::CollisionReturn data, GameObject::SideCollisions side);

	void OnNoCollisionWithBottom();

	MoveDirection getMoveDirection();

	RECT GetBound();

	AladdinState::StateName Aladdin::getState();

	//xu ly input
	//gom 256 key tuong ung true = dang dc nhan, false = khong dc nhan
	void HandleKeyboard(std::map<int, bool> keys);

	void OnKeyPressed(int key);

	void OnKeyUp(int key);

	//true thi se lat nguoc anh theo truc y
	void SetReverse(bool flag);

	bool allowMoveLeft;
	bool allowMoveRight;

	Animation * getCurrentAnimation();

	BulletApple *_Bullet[MAX_BULLET_APPLE];
	int _nBullet = 0;
	bool _throwBullet = false;
	int _totalApple = 10;
	float _timeToThrow = 0;

	bool lastRightDirection = true;

	bool isHurt = false;
	float ellapsedTime = 0.0f;

protected:
	Camera      *mCamera;

	AladdinData *mAladdinData;

	Animation   *mCurrentAnimation,
				*mAnimationStanding,
				*mAnimationStandingIdle2,
				*mAnimationStandingIdle3,
				*mAnimationStandingFight,
				*mAnimationStandingFightWithApple,
				*mAnimationRunning,
				*mAnimationRunningJump,
				*mAnimationJumping,
				*mAnimationSitting,
				*mAnimationSittingThrow,
				*mAnimationSittingFight,
				*mAnimationClimbing,
				*mAnimationClimbingUp,
				*mAnimationClimbingLeft,
				*mAnimationClimbingFight,
				*mAnimationClimbingFightWithApple,
				*mAnimationPushing,
				*mAnimationDie;

	void changeAnimation(AladdinState::StateName state);

	AladdinState::StateName mCurrentState;

	std::vector<BulletApple*> mListBulletApples;

	//chi cho phep jump khi nhan nhim space, muon nhay lai phai tha phim space roi nhan lai
	bool allowJump, mCurrentReverse;
};

