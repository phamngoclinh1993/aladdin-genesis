#include "AladdinSittingFightWithAppleState.h"
#include "AladdinSittingState.h"


AladdinSittingFightWithAppleState::AladdinSittingFightWithAppleState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinSittingFightWithAppleState::~AladdinSittingFightWithAppleState()
{
}

void AladdinSittingFightWithAppleState::Update(float dt)
{

}

void AladdinSittingFightWithAppleState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_DOWN])
	{
		if (!keys[0x41])
		{
			this->mAladdinData->aladdin->SetState(new AladdinSittingState(this->mAladdinData));
			return;
		}
		else
		{
			// Throw apple
		}
	}
	else
	{
		this->mAladdinData->aladdin->SetState(new AladdinSittingState(this->mAladdinData));
		return;
	}
}

void AladdinSittingFightWithAppleState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinSittingFightWithAppleState::GetState()
{
	return AladdinState::SittingFightWithApple;
}