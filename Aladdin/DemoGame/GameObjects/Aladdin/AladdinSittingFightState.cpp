#include "AladdinSittingFightState.h"
#include "AladdinSittingState.h"


AladdinSittingFightState::AladdinSittingFightState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinSittingFightState::~AladdinSittingFightState()
{
}


void AladdinSittingFightState::Update(float dt)
{

}

void AladdinSittingFightState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_DOWN])
	{
		if (!keys[0x58])
		{
			this->mAladdinData->aladdin->SetState(new AladdinSittingState(this->mAladdinData));
			return;
		}
		else
		{
			// Fight
		}
	}
	else
	{
		this->mAladdinData->aladdin->SetState(new AladdinSittingState(this->mAladdinData));
		return;
	}
}

void AladdinSittingFightState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinSittingFightState::GetState()
{
	return AladdinState::SittingFight;
}
