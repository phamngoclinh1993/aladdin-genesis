#include "AladdinStandingIdle2State.h"
#include "AladdinStandingIdle3State.h"
#include "AladdinStandingFightState.h"
#include "AladdinRunningState.h"
#include "AladdinJumpingState.h"
#include "AladdinSittingState.h"


AladdinStandingIdle2State::AladdinStandingIdle2State(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinStandingIdle2State::~AladdinStandingIdle2State()
{
}


void AladdinStandingIdle2State::Update(float dt)
{
	if (elapsedTime > 10.0f)
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingIdle3State(this->mAladdinData));
		return;
	}
	else
	{
		elapsedTime += dt;
	}
}

void AladdinStandingIdle2State::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_LEFT] || keys[VK_RIGHT])
	{
		this->mAladdinData->aladdin->SetState(new AladdinRunningState(this->mAladdinData));
		return;
	}
	else if (keys[VK_SPACE])
	{
		this->mAladdinData->aladdin->SetState(new AladdinJumpingState(this->mAladdinData));
		return;
	}
	else if (keys[VK_DOWN])
	{
		this->mAladdinData->aladdin->SetState(new AladdinSittingState(this->mAladdinData));
		return;
	}
	else if (keys[0x58])
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingFightState(this->mAladdinData));
		return;
	}
}

void AladdinStandingIdle2State::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinStandingIdle2State::GetState()
{
	return AladdinState::StandingIdle2;
}
