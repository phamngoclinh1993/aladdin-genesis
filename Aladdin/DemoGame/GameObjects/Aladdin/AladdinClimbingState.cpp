#include "AladdinClimbingState.h"
#include "AladdinClimbingLeftState.h"
#include "AladdinClimbingUpState.h"
#include "AladdinClimbingFightState.h"
#include "AladdinClimbingFightWithAppleState.h"


AladdinClimbingState::AladdinClimbingState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinClimbingState::~AladdinClimbingState()
{
}


void AladdinClimbingState::Update(float dt)
{

}

void AladdinClimbingState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_UP] || keys[VK_DOWN])
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingUpState(this->mAladdinData));
		return;
	}
	else if (keys[VK_LEFT] || keys[VK_RIGHT])
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingLeftState(this->mAladdinData));
		return;
	}
	else if (keys[0x58])
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingFightState(this->mAladdinData));
		return;
	}
	else if (keys[0x41])
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingFightWithAppleState(this->mAladdinData));
		return;
	}
	else
	{
	}
}

void AladdinClimbingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinClimbingState::GetState()
{
	return AladdinState::Climbing;
}