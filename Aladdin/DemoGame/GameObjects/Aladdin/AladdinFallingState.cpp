#include "AladdinFallingState.h"
#include "AladdinRunningState.h"
#include "AladdinStandingState.h"
#include "..\..\Components\Collision.h"
#include "..\..\Components\GameLog.h"

AladdinFallingState::AladdinFallingState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;

	acceleratorY = 15.0f;
	acceleratorX = 8.0f;

	if (this->mAladdinData->aladdin->GetVx() == 0)
	{
		allowMoveX = false;
	}
	else
	{
		allowMoveX = true;
	}
}


AladdinFallingState::~AladdinFallingState()
{

}

void AladdinFallingState::Update(float dt)
{
	this->mAladdinData->aladdin->AddVy(acceleratorY);

	if (mAladdinData->aladdin->GetVy() > 480.0f)
	{
		mAladdinData->aladdin->SetVy(480.0f);
	}
}

void AladdinFallingState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_RIGHT])
	{
		mAladdinData->aladdin->SetReverse(false);

		isLeftOrRightKeyPressed = true;
		//di chuyen sang phai
		if (this->mAladdinData->aladdin->GetVx() < 150.0f)
		{
			this->mAladdinData->aladdin->AddVx(acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() >= 150.0f)
			{
				this->mAladdinData->aladdin->SetVx(150.0f);
			}
		}
	}
	else if (keys[VK_LEFT])
	{
		mAladdinData->aladdin->SetReverse(true);

		isLeftOrRightKeyPressed = true;
		//di chuyen sang trai
		if (this->mAladdinData->aladdin->GetVx() > -150.0f)
		{
			this->mAladdinData->aladdin->AddVx(-acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() <= -150.0f)
			{
				this->mAladdinData->aladdin->SetVx(-150.0f);
			}
		}
	}
	else
	{
		isLeftOrRightKeyPressed = false;
	}
}

void AladdinFallingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	switch (side)
	{
	case GameObject::Left:
		//GAMELOG("FALLING COLLISION LEFT");
		break;

	case GameObject::Right:
		//GAMELOG("FALLING COLLISION RIGHT");
		break;

	case GameObject::Top:
		//GAMELOG("FALLING COLLISION TOP");
		break;

	case GameObject::Bottom:
	case GameObject::BottomRight:
	case GameObject::BottomLeft:
		//GAMELOG("FALLING COLLISION BOTTOM - BOTTOM RIGHT - BOTTOM LEFT");
		if (impactor->Tag == GameObject::ObjectTypes::Earth
			|| impactor->Tag == GameObject::ObjectTypes::WoodenLadder
			|| impactor->Tag == GameObject::ObjectTypes::Static)
		{
			if (data.RegionCollision.right - data.RegionCollision.left >= 10.0f)
			{
				this->mAladdinData->aladdin->AddPosition(0, -(data.RegionCollision.bottom - data.RegionCollision.top));

				/*if (isLeftOrRightKeyPressed)
				{
					this->mAladdinData->aladdin->SetState(new AladdinRunningState(this->mAladdinData));
				}
				else
				{
					this->mAladdinData->aladdin->SetState(new AladdinStandingState(this->mAladdinData));
				}*/

				this->mAladdinData->aladdin->SetState(new AladdinStandingState(this->mAladdinData));
			}
		}
		return;

	default:
		break;
	}
}

AladdinState::StateName AladdinFallingState::GetState()
{
	return AladdinState::Falling;
}
