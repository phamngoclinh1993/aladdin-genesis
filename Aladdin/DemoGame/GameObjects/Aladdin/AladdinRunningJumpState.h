#pragma once

#include "AladdinState.h";
#include "Aladdin.h"

class AladdinRunningJumpState : public AladdinState
{
public:
	AladdinRunningJumpState(AladdinData *aladdinData);
	~AladdinRunningJumpState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	float acceleratorY;
	float acceleratorX;
	bool noPressed;
	bool allowMoveRight, allowMoveLeft;
};

