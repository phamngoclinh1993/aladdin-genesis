#pragma once

#include "AladdinState.h"
#include "Aladdin.h"

class AladdinStandingFightWithAppleState : public AladdinState
{
public:
	AladdinStandingFightWithAppleState(AladdinData *aladdinData);
	~AladdinStandingFightWithAppleState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	float elapsedTime = 0.0f;
	bool isThrow = true;
};

