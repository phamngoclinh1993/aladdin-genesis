#include "AladdinDieingState.h"
#include "AladdinRunningState.h"
#include "AladdinStandingState.h"
#include "..\..\Components\Collision.h"
#include "..\..\Components\GameLog.h"

AladdinDieingState::AladdinDieingState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinDieingState::~AladdinDieingState()
{

}

void AladdinDieingState::Update(float dt)
{
	
}

void AladdinDieingState::HandleKeyboard(std::map<int, bool> keys)
{
	
}

void AladdinDieingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	
}

AladdinState::StateName AladdinDieingState::GetState()
{
	return AladdinState::Die;
}
