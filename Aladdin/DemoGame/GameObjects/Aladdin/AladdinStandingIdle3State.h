#pragma once

#include "AladdinState.h"
#include "Aladdin.h"

class AladdinStandingIdle3State : public AladdinState
{
public:
	AladdinStandingIdle3State(AladdinData *aladdinData);
	~AladdinStandingIdle3State();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:

	float elapsedTime = 0.0f;
};

