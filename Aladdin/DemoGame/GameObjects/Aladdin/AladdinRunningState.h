#pragma once

#include "AladdinState.h"
#include "Aladdin.h"

class AladdinRunningState : public AladdinState
{
public:
	AladdinRunningState(AladdinData *aladdinData);
	~AladdinRunningState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	//do bien tien van toc sau moi frame tinh bang pixel / s
	float acceleratorX;
};

