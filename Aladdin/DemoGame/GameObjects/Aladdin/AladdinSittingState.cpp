#include "AladdinSittingState.h"
#include "AladdinSittingFightWithAppleState.h"
#include "AladdinSittingFightState.h"
#include "AladdinStandingState.h"


AladdinSittingState::AladdinSittingState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinSittingState::~AladdinSittingState()
{
}


void AladdinSittingState::Update(float dt)
{
	int currentIndex = this->mAladdinData->aladdin->getCurrentAnimation()->getCurrentIndex();
	if (currentIndex >= 3)
		this->mAladdinData->aladdin->getCurrentAnimation()->setOneLoop(true);
		
}

void AladdinSittingState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_DOWN])
	{
		if (keys[0x41])
		{
			this->mAladdinData->aladdin->SetState(new AladdinSittingFightWithAppleState(this->mAladdinData));
			return;
		}
		else if (keys[0x58])
		{
			this->mAladdinData->aladdin->SetState(new AladdinSittingFightState(this->mAladdinData));
			return;
		}
	}
	else
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingState(this->mAladdinData));
		return;
	}
}

void AladdinSittingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinSittingState::GetState()
{
	return AladdinState::Sitting;
}
