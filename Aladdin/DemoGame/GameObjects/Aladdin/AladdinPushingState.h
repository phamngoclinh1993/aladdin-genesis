#pragma once
#include "AladdinState.h"

class AladdinPushingState : public AladdinState
{
public:
	AladdinPushingState(AladdinData *aladdinData);
	~AladdinPushingState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	bool allowJump;

	float elapsedTime = 0.0f;
};

