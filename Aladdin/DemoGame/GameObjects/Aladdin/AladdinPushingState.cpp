#include "AladdinPushingState.h"
#include "AladdinStandingState.h"
#include "AladdinStandingFightState.h"
#include "AladdinStandingFightWithAppleState.h"
#include "AladdinRunningState.h"
#include "AladdinJumpingState.h"
#include "AladdinSittingState.h"
#include "AladdinClimbingState.h"
#include "Aladdin.h"

AladdinPushingState::AladdinPushingState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
	this->mAladdinData->aladdin->SetVx(0);
	this->mAladdinData->aladdin->SetVy(0);
}


AladdinPushingState::~AladdinPushingState()
{
}

void AladdinPushingState::Update(float dt)
{
	
}

void AladdinPushingState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_LEFT] || keys[VK_RIGHT])
	{
		//this->mAladdinData->aladdin->SetState(new AladdinRunningState(this->mAladdinData));
		return;
	}
	else if (keys[VK_SPACE])
	{
		this->mAladdinData->aladdin->SetState(new AladdinJumpingState(this->mAladdinData));
		return;
	}
	else if (keys[VK_DOWN])
	{
		this->mAladdinData->aladdin->SetState(new AladdinSittingState(this->mAladdinData));
		return;
	}
	else if (keys[0x58])
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingFightState(this->mAladdinData));
		return;
	}
	else if (keys[0x41])
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingFightWithAppleState(this->mAladdinData));
		return;
	}

	// Example climbing state
	else if (keys[VK_CONTROL])
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingState(this->mAladdinData));
		return;
	}
}

void AladdinPushingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	
}

AladdinState::StateName AladdinPushingState::GetState()
{
	return AladdinState::Pushing;
}
