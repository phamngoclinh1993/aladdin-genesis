#include "AladdinStandingIdle3State.h"
#include "AladdinStandingFightState.h"
#include "AladdinRunningState.h"
#include "AladdinJumpingState.h"
#include "AladdinSittingState.h"


AladdinStandingIdle3State::AladdinStandingIdle3State(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinStandingIdle3State::~AladdinStandingIdle3State()
{
}

void AladdinStandingIdle3State::Update(float dt)
{
	
}

void AladdinStandingIdle3State::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_LEFT] || keys[VK_RIGHT])
	{
		this->mAladdinData->aladdin->SetState(new AladdinRunningState(this->mAladdinData));
		return;
	}
	else if (keys[VK_SPACE])
	{
		this->mAladdinData->aladdin->SetState(new AladdinJumpingState(this->mAladdinData));
		return;
	}
	else if (keys[VK_DOWN])
	{
		this->mAladdinData->aladdin->SetState(new AladdinSittingState(this->mAladdinData));
		return;
	}
	else if (keys[0x58])
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingFightState(this->mAladdinData));
		return;
	}
}

void AladdinStandingIdle3State::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinStandingIdle3State::GetState()
{
	return AladdinState::StandingIdle3;
}
