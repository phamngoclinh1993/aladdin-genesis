#pragma once
#include "Aladdin.h"
#include "AladdinState.h"

class AladdinJumpingState : public AladdinState
{
public:
	AladdinJumpingState(AladdinData *aladdinData);
	~AladdinJumpingState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	bool isJumpDone = false;
	//do bien tien van toc sau moi frame tinh bang pixel / s
	float acceleratorY;
	float acceleratorX;
	bool noPressed;
	bool allowMoveRight, allowMoveLeft;
};

