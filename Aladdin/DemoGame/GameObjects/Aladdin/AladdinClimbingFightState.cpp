#include "AladdinClimbingFightState.h"
#include "AladdinClimbingState.h"

AladdinClimbingFightState::AladdinClimbingFightState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinClimbingFightState::~AladdinClimbingFightState()
{
}



void AladdinClimbingFightState::Update(float dt)
{

}

void AladdinClimbingFightState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[0x58])
	{
		
	}
	else
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingState(this->mAladdinData));
		return;
	}
}

void AladdinClimbingFightState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinClimbingFightState::GetState()
{
	return AladdinState::ClimbingFight;
}