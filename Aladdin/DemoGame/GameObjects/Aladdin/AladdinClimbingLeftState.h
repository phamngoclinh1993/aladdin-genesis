#pragma once

#include "AladdinState.h"
#include "Aladdin.h"

class AladdinClimbingLeftState : public AladdinState
{
public:
	AladdinClimbingLeftState(AladdinData *aladdinData);
	~AladdinClimbingLeftState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
	int currentFrame;
};

