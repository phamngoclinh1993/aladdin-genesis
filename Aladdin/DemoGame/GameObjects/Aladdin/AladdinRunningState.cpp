#include "AladdinRunningState.h"
#include "AladdinRunningJumpState.h"
#include "AladdinStandingState.h"
#include "AladdinPushingState.h"
#include "..\..\Components\GameLog.h"

AladdinRunningState::AladdinRunningState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;

	acceleratorX = 14.0f;
}


AladdinRunningState::~AladdinRunningState()
{
}

void AladdinRunningState::Update(float dt)
{

}

void AladdinRunningState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_RIGHT])
	{
		mAladdinData->aladdin->SetReverse(false);

		//di chuyen sang phai
		if (this->mAladdinData->aladdin->GetVx() < 150.0f)
		{
			this->mAladdinData->aladdin->AddVx(acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() >= 150.0f)
			{
				this->mAladdinData->aladdin->SetVx(150.0f);
			}
		}

		if (keys[VK_SPACE])
		{
			this->mAladdinData->aladdin->SetState(new AladdinRunningJumpState(this->mAladdinData));
			return;
		}
	}
	else if (keys[VK_LEFT])
	{
		mAladdinData->aladdin->SetReverse(true);

		//di chuyen sang trai
		if (this->mAladdinData->aladdin->GetVx() > -150.0f)
		{
			this->mAladdinData->aladdin->AddVx(-acceleratorX);

			if (this->mAladdinData->aladdin->GetVx() < -150.0f)
			{
				this->mAladdinData->aladdin->SetVx(-150.0f);
			}
		}

		if (keys[VK_SPACE])
		{
			this->mAladdinData->aladdin->SetState(new AladdinRunningJumpState(this->mAladdinData));
			return;
		}
	}
	else
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingState(this->mAladdinData));
		return;
	}
}

void AladdinRunningState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	switch (side)
	{
		case GameObject::Left:
		{
			// va cham phia ben trai cua player
			if (this->mAladdinData->aladdin->getMoveDirection() == Aladdin::MoveToLeft)
			{
				if (impactor->Tag == GameObject::ObjectTypes::Hakim ||
					impactor->Tag == GameObject::ObjectTypes::Boxer ||
					impactor->Tag == GameObject::ObjectTypes::Razoul)
				{
					//GAMELOG("LEFT");
				}
				else if (impactor->Tag == GameObject::ObjectTypes::Wall)
				{
					this->mAladdinData->aladdin->SetState(new AladdinPushingState(this->mAladdinData));
				}
			}

			return;
		}

		case GameObject::Right:
		{
			//va cham phia ben phai player
			if (this->mAladdinData->aladdin->getMoveDirection() == Aladdin::MoveToRight)
			{
				if (impactor->Tag == GameObject::ObjectTypes::Hakim ||
					impactor->Tag == GameObject::ObjectTypes::Boxer ||
					impactor->Tag == GameObject::ObjectTypes::Razoul)
				{
					//GAMELOG("RUNNING COLLISION RIGHT");
				}
				else if (impactor->Tag == GameObject::ObjectTypes::Wall)
				{
					this->mAladdinData->aladdin->SetState(new AladdinPushingState(this->mAladdinData));
				}
			}
			return;
		}

		case GameObject::Top:
		{
			if (impactor->Tag == GameObject::ObjectTypes::Hakim ||
				impactor->Tag == GameObject::ObjectTypes::Boxer ||
				impactor->Tag == GameObject::ObjectTypes::Razoul)
			{
				//GAMELOG("RUNNING COLLISION TOP");
			}
			break;
		}

		case GameObject::Bottom: case GameObject::BottomLeft: case GameObject::BottomRight:
		{
			if (impactor->Tag == GameObject::ObjectTypes::Hakim ||
				impactor->Tag == GameObject::ObjectTypes::Boxer ||
				impactor->Tag == GameObject::ObjectTypes::Razoul)
			{
				//GAMELOG("RUNNING COLLISION  BOTTOM - BOTTOM LEFT - BOTTOM RIGHT");
			}
			else if (impactor->Tag == GameObject::ObjectTypes::Earth 
				|| impactor->Tag == GameObject::ObjectTypes::WoodenLadder
				|| impactor->Tag == GameObject::ObjectTypes::Static)
			{
				this->mAladdinData->aladdin->AddPosition(0, -(data.RegionCollision.bottom - data.RegionCollision.top));

				this->mAladdinData->aladdin->SetVy(0);
			}
			return;
		}
	}
}

AladdinState::StateName AladdinRunningState::GetState()
{
	return AladdinState::Running;
}
