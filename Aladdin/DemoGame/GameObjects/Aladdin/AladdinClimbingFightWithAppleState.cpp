#include "AladdinClimbingFightWithAppleState.h"
#include "AladdinClimbingState.h"


AladdinClimbingFightWithAppleState::AladdinClimbingFightWithAppleState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
}


AladdinClimbingFightWithAppleState::~AladdinClimbingFightWithAppleState()
{
}

void AladdinClimbingFightWithAppleState::Update(float dt)
{

}

void AladdinClimbingFightWithAppleState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[0x41])
	{

	}
	else
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingState(this->mAladdinData));
		return;
	}
}

void AladdinClimbingFightWithAppleState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{

}

AladdinState::StateName AladdinClimbingFightWithAppleState::GetState()
{
	return AladdinState::ClimbingFightWithApple;
}