#include "AladdinStandingState.h"
#include "AladdinStandingIdle2State.h"
#include "AladdinStandingFightState.h"
#include "AladdinStandingFightWithAppleState.h"
#include "AladdinRunningState.h"
#include "AladdinJumpingState.h"
#include "AladdinSittingState.h"
#include "AladdinClimbingState.h"
#include "Aladdin.h"

AladdinStandingState::AladdinStandingState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
	this->mAladdinData->aladdin->SetVx(0);
	this->mAladdinData->aladdin->SetVy(0);
}


AladdinStandingState::~AladdinStandingState()
{
}

void AladdinStandingState::Update(float dt)
{
	this->mAladdinData->aladdin->setWasAttacked(false);
	if (elapsedTime > 10.0f)
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingIdle2State(this->mAladdinData));
		return;
	}
	else
	{
		elapsedTime += dt;
	}
}

void AladdinStandingState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[VK_LEFT] || keys[VK_RIGHT])
	{
		this->mAladdinData->aladdin->SetState(new AladdinRunningState(this->mAladdinData));
		return;
	}
	else if (keys[VK_SPACE])
	{
		this->mAladdinData->aladdin->SetState(new AladdinJumpingState(this->mAladdinData));
		return;
	}
	else if (keys[VK_DOWN])
	{
		this->mAladdinData->aladdin->SetState(new AladdinSittingState(this->mAladdinData));
		return;
	}
	else if (keys[0x58])
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingFightState(this->mAladdinData));
		return;
	}
	else if (keys[0x41])
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingFightWithAppleState(this->mAladdinData));
		return;
	}

	// Example climbing state
	else if (keys[VK_CONTROL])
	{
		this->mAladdinData->aladdin->SetState(new AladdinClimbingState(this->mAladdinData));
		return;
	}
}

void AladdinStandingState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	
}

AladdinState::StateName AladdinStandingState::GetState()
{
	return AladdinState::Standing;
}
