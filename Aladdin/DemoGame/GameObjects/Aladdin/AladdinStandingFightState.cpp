#include "AladdinStandingFightState.h"
#include "AladdinStandingState.h"


AladdinStandingFightState::AladdinStandingFightState(AladdinData *aladdinData)
{
	this->mAladdinData = aladdinData;
	this->mAladdinData->aladdin->setWasAttacked(true);
}


AladdinStandingFightState::~AladdinStandingFightState()
{
}

void AladdinStandingFightState::Update(float dt)
{

}

void AladdinStandingFightState::HandleKeyboard(std::map<int, bool> keys)
{
	if (keys[0x58])
	{
		/*this->mAladdinData->aladdin->SetState(new AladdinStandingFightState(this->mAladdinData));
		return;*/
	}
	else
	{
		this->mAladdinData->aladdin->SetState(new AladdinStandingState(this->mAladdinData));
		return;
	}
}

void AladdinStandingFightState::OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data)
{
	if (impactor->Tag == GameObject::ObjectTypes::Hakim)
	{
		if (!impactor->IsDead())
		{
			impactor->GetAttack();
		}
		else
		{
			impactor->Dead();
			impactor = NULL;
		}
	}
}

AladdinState::StateName AladdinStandingFightState::GetState()
{
	return AladdinState::StandingFight;
}