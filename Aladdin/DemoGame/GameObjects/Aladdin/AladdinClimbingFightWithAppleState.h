#pragma once

#include "AladdinState.h"
#include "Aladdin.h"

class AladdinClimbingFightWithAppleState : public AladdinState
{
public:
	AladdinClimbingFightWithAppleState(AladdinData *aladdinData);
	~AladdinClimbingFightWithAppleState();

	void Update(float dt);

	void HandleKeyboard(std::map<int, bool> keys);

	void OnCollision(GameObject *impactor, GameObject::SideCollisions side, GameObject::CollisionReturn data);

	virtual StateName GetState();

protected:
};

