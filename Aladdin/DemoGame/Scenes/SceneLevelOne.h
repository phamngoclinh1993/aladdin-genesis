#pragma once

#include <math.h>
#include <vector>
#include <d3dx9.h>
#include <d3d9.h>

#include "..\Components\Scene.h"
#include "..\Components\Sprite.h"
#include "..\Components\Camera.h"
#include "..\Managements\SceneManagement.h"
#include "SceneBoss.h"
#include "..\Components\Map.h"

#include "..\GameObjects\Aladdin\Aladdin.h"
#include "..\GameObjects\Enemies\Camel\Camel.h"
#include "..\GameObjects\Enemies\Boxer\Boxer.h"
#include "..\GameObjects\Enemies\Hakim\Hakim.h"
#include "..\GameObjects\Enemies\Razoul\Razoul.h"
#include "..\GameObjects\Enemies\Knife\Knife.h"
#include "..\GameObjects\Enemies\Hand\Hand.h"
#include "..\Bullets\BulletApple.h"

class SceneLevelOne : public Scene
{
public:
	SceneLevelOne();
	~SceneLevelOne();

	void Update(float dt);
	void LoadContent();
	void Draw();

	void OnKeyDown(int keyCode);
	void OnKeyUp(int keyCode);
	void OnMouseDown(float x, float y);

protected:
	void checkCollision();
	void CheckCameraAndWorldMap();

	Map *mMap;

	Camera *mCamera;
	Aladdin *mAladdin;

	//Enemies
	/*Knife *mKnife;
	Hand *mHand;*/
	Hakim *mHakim;
	Razoul *mRazoul;
	Boxer *mBoxer;
	float mTimeCounter;

	std::map<int, bool> keys;
};

