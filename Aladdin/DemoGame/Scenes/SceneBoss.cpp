#include "SceneBoss.h"


SceneBoss::SceneBoss()
{
	LoadContent();
}


SceneBoss::~SceneBoss()
{
}

void SceneBoss::LoadContent()
{
	// set time counter = 0
	mTimeCounter = 0;

	// set mau backcolor cho scene o day la mau xanh
	mBackColor = 0x54acd2;

	mAnimation = new Animation("Resources/cat.png", 37, 5, 8, 0.15f);
	mAnimation->SetPosition(400, 200);
}

void SceneBoss::Update(float dt)
{
	mAnimation->Update(dt);
}

void SceneBoss::Draw()
{
	mAnimation->Draw();
}

void SceneBoss::OnKeyDown(int keyCode)
{
	if (keyCode == VK_LEFT)
	{
		SceneManagement::GetInstance()->ReplaceScene(new SceneLevelOne());
	}

	if (keyCode == VK_RIGHT)
	{

	}

	if (keyCode == VK_UP)
	{

	}

	if (keyCode == VK_DOWN)
	{

	}
}

void SceneBoss::OnKeyUp(int keyCode)
{

}

void SceneBoss::OnMouseDown(float x, float y)
{
}