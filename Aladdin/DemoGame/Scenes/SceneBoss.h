#pragma once

#include <math.h>
#include <vector>
#include <d3dx9.h>
#include <d3d9.h>

#include "..\Components\Scene.h"
#include "..\Components\Animation.h"
#include "..\Managements\SceneManagement.h"
#include "SceneLevelOne.h"

class SceneBoss : public Scene
{
public:
	SceneBoss();
	~SceneBoss();

	void Update(float dt);
	void LoadContent();
	void Draw();

	void OnKeyDown(int keyCode);
	void OnKeyUp(int keyCode);
	void OnMouseDown(float x, float y);
	 
protected:
	Animation *mAnimation;

	float mTimeCounter;
};

