#include "SceneLevelOne.h"
#include "..\Components\GameLog.h"


SceneLevelOne::SceneLevelOne()
{
	LoadContent();
}


SceneLevelOne::~SceneLevelOne()
{
}

void SceneLevelOne::LoadContent()
{
	mTimeCounter = 0;
	mBackColor = 0x54acd2;

	mMap = new Map("Resources/map/SceneOne/mapa.tmx");

	mCamera = new Camera(GameGlobal::GetWidth(), GameGlobal::GetHeight());
	mCamera->SetPosition(GameGlobal::GetWidth() / 2, mMap->GetHeight() - GameGlobal::GetHeight() / 2);

	mMap->SetCamera(mCamera);

	mAladdin = mMap->getAladdin();
}

void SceneLevelOne::Update(float dt)
{
	mMap->Update(dt);

	checkCollision();

	mAladdin->HandleKeyboard(keys);
	mAladdin->Update(dt);

	CheckCameraAndWorldMap();
}

void SceneLevelOne::Draw()
{
	mMap->Draw();
}

void SceneLevelOne::OnKeyDown(int keyCode)
{
	keys[keyCode] = true;

	mAladdin->OnKeyPressed(keyCode);
}

void SceneLevelOne::OnKeyUp(int keyCode)
{
	keys[keyCode] = false;

	mAladdin->OnKeyUp(keyCode);
}

void SceneLevelOne::OnMouseDown(float x, float y)
{
}

void SceneLevelOne::CheckCameraAndWorldMap()
{
	mCamera->SetPosition(mAladdin->GetPosition());

	if (mCamera->GetBound().left < 0)
	{
		//vi position cua camera ma chinh giua camera
		//luc nay o vi tri goc ben trai cua the gioi thuc
		mCamera->SetPosition(mCamera->GetWidth() / 2, mCamera->GetPosition().y);
	}

	if (mCamera->GetBound().right > mMap->GetWidth())
	{
		//luc nay cham goc ben phai cua the gioi thuc
		mCamera->SetPosition(mMap->GetWidth() - mCamera->GetWidth() / 2, mCamera->GetPosition().y);
	}

	if (mCamera->GetBound().top < 0)
	{
		//luc nay cham goc tren the gioi thuc
		mCamera->SetPosition(mCamera->GetPosition().x, mCamera->GetHeight() / 2);
	}

	if (mCamera->GetBound().bottom > mMap->GetHeight())
	{
		//luc nay cham day cua the gioi thuc
		mCamera->SetPosition(mCamera->GetPosition().x, mMap->GetHeight() - mCamera->GetHeight() / 2);
	}
}

void SceneLevelOne::checkCollision()
{
	/*su dung de kiem tra xem khi nao aladdin khong dung tren 1 object hoac
	dung qua sat mep trai hoac phai cua object do thi se chuyen state la falling*/
	int widthBottom = 0;

	vector<GameObject*> listCollision;

	mMap->GetQuadTree()->getObjectsCollideAble(listCollision, mAladdin);

	for (size_t i = 0; i < listCollision.size(); i++)
	{
		GameObject::CollisionReturn r = Collision::RecteAndRect(mAladdin->GetBound(),
			listCollision.at(i)->GetBound());

		if (r.IsCollided)
		{
			//lay phia va cham cua Entity so voi Player
			GameObject::SideCollisions sidePlayer = Collision::getSideCollision(mAladdin, r);

			//lay phia va cham cua Player so voi Entity
			GameObject::SideCollisions sideImpactor = Collision::getSideCollision(listCollision.at(i), r);

			//goi den ham xu ly collision cua Player va Entity
			if (listCollision.at(i)->Tag != GameObject::ObjectTypes::Aladdin)
			{
				mAladdin->OnCollision(listCollision.at(i), r, sidePlayer);
			}
			listCollision.at(i)->OnCollision(mAladdin, r, sideImpactor);

			//kiem tra neu va cham voi phia duoi cua Player 
			if (sidePlayer == GameObject::Bottom || sidePlayer == GameObject::BottomLeft
				|| sidePlayer == GameObject::BottomRight)
			{
				//kiem cha do dai ma aladdin tiep xuc phia duoi day
				int bot = r.RegionCollision.right - r.RegionCollision.left;

				if (bot > widthBottom)
					widthBottom = bot;
			}
		}
	}

	//Neu aladdin dung ngoai mep thi luc nay cho aladdin rot xuong duoi dat    
	if (widthBottom < 6.0f)
	{
		mAladdin->OnNoCollisionWithBottom();
	}



	// Check collision with bullet
	for (size_t b = 0; b < MAX_BULLET_APPLE; b++)
	{
		if (mAladdin->_Bullet[b] != NULL && mAladdin->_Bullet[b]->_isDead == false)
		{
			mMap->GetQuadTree()->insertObject(mAladdin->_Bullet[b]);
			vector<GameObject*> listCollisionWithApple;

			mMap->GetQuadTree()->getObjectsCollideAble(listCollisionWithApple, mAladdin->_Bullet[b]);

			for (size_t i = 0; i < listCollisionWithApple.size(); i++)
			{
				GameObject::CollisionReturn r = Collision::RecteAndRect(mAladdin->_Bullet[b]->GetBound(),
					listCollisionWithApple.at(i)->GetBound());

				if (r.IsCollided)
				{
					//lay phia va cham cua Entity so voi Bullet
					GameObject::SideCollisions sidePlayer = Collision::getSideCollision(mAladdin->_Bullet[b], r);

					//lay phia va cham cua Bullet so voi Entity
					GameObject::SideCollisions sideImpactor = Collision::getSideCollision(listCollisionWithApple.at(i), r);

					//goi den ham xu ly collision cua Bullet va Entity
					mAladdin->_Bullet[b]->OnCollision(listCollisionWithApple.at(i), r, sidePlayer);
					listCollisionWithApple.at(i)->OnCollision(mAladdin->_Bullet[b], r, sideImpactor);
				}
			}
		}
	}
}