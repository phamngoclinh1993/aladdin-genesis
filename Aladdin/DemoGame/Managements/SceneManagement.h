#ifndef __SCENE_MANAGER__
#define __SCENE_MANAGER__

#include "..\Components\Scene.h"

class SceneManagement
{
public:
	static SceneManagement *GetInstance();
	~SceneManagement();

	Scene* GetCurrentScene();
	void Update(float dt);
	void ReplaceScene(Scene *scene);

private:
	SceneManagement();
	static SceneManagement		*mInstace;
	Scene						*mCurrentScene;
};

#endif