#include "SceneManagement.h"

SceneManagement* SceneManagement::mInstace = NULL;

SceneManagement::SceneManagement()
{
	mCurrentScene = nullptr;
}

SceneManagement::~SceneManagement()
{
}

SceneManagement* SceneManagement::GetInstance()
{
	if (!mInstace)
		mInstace = new SceneManagement();

	return mInstace;
}

Scene* SceneManagement::GetCurrentScene()
{
	return mCurrentScene;
}

void SceneManagement::Update(float dt)
{
	mCurrentScene->Update(dt);
}

void SceneManagement::ReplaceScene(Scene *scene)
{
	delete mCurrentScene;

	mCurrentScene = scene;
}